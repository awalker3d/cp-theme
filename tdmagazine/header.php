<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package tdmagazine
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site container">

	<?php tdmagazine_ad_section( 'top' ); ?>

	<div class="site-inner">
		<header id="masthead" class="site-header" role="banner">

			<?php if( has_nav_menu( 'top-menu' ) ||  has_nav_menu( 'social' ) ): ?>
			<div class="top-section clearfix">
				<nav id="secondary-navigation" class="top-navigation pull-left" role="navigation">
					<?php wp_nav_menu( array( 'container' => 'ul', 'menu_class' => 'nav-bar', 'theme_location' => 'top-menu') ); ?>
				</nav><!-- .top-navigation -->

				<?php if( tdmagazine_is_social_top() ): ?>
				<div class="header-social pull-right">
					<?php get_template_part( 'menu', 'social' ); ?>
				</div><!-- .header-social -->
				<?php endif; ?>
			</div><!-- .top-section -->
			<?php endif; ?>

			<div class="clearfix">
				<div class="site-branding pull-left">

					<?php if( get_theme_mod('tdmagazine_header_logo_img', '') != '' ): ?>
					<div class="website-logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img src="<?php echo esc_url( get_theme_mod('tdmagazine_header_logo_img', '') ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
						</a>
					</div><!-- .website-logo -->
					<?php endif; ?>

					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
				</div><!-- .site-branding -->
				<form method="get" id="header-searchform" class="searchform form-inline pull-right" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
					<input type="text" class="field" name="s" value="<?php if( get_search_query() ) { echo esc_attr( get_search_query() ); } else { esc_attr_e( 'To search type and hit enter', 'tdmagazine' ); } ?>" id="s" onfocus="if(this.value=='<?php esc_attr_e('To search type and hit enter', 'tdmagazine' ); ?>')this.value='';" onblur="if(this.value=='')this.value='<?php esc_attr_e( 'To search type and hit enter', 'tdmagazine' ); ?>';" />
					<div class="search-box-icon"><i class="fa fa-search"></i></div>
				</form><!-- #header-searchform -->
			</div><!-- .clearfix -->

			<?php if ( has_nav_menu( 'main-menu' ) ): ?>
			<div id="mobile-site-navigation"></div><!-- #mobile-site-navigation -->

			<nav class="main-navigation clearfix" role="navigation" data-small-nav-title="<?php _e( 'Menu', 'tdmagazine' ); ?>">
				<?php wp_nav_menu( array( 'container' => 'ul', 'menu_class' => 'nav-bar clearfix', 'theme_location' => 'main-menu') ); ?>
			</nav><!-- .main-navigation -->
			<?php endif; ?>

			<?php tdmagazine_breaking_news(); ?>
		</header><!-- #masthead -->

		<div id="main" class="site-main">
			<?php tdmagazine_breadcrumbs(); ?>

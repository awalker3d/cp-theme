<?php
/**
 * The template for displaying search forms in tdmagazine
 *
 * @package tdmagazine
 */
?>
	<form method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
		<input type="text" class="field" name="s" value="<?php if( get_search_query() ) { echo esc_attr( get_search_query() ); } else { _e( 'To search type and hit enter', 'tdmagazine' ); } ?>" id="s" onfocus="if(this.value=='<?php esc_attr_e( __( 'To search type and hit enter', 'tdmagazine' ) ); ?>')this.value='';" onblur="if(this.value=='')this.value='<?php esc_attr_e( __( 'To search type and hit enter', 'tdmagazine' ) ); ?>';" />
		<div class="search-box-icon"><i class="fa fa-search"></i></div>
	</form>

<?php
/**
 * Breadcrumbs
 *
 * @since tdmagazine 1.0
 * @updated tdmagazine 2.0
 */
function tdmagazine_breadcrumbs() {
	$is_breadcrumbs = get_theme_mod( 'tdmagazine_website_settings_breadcrumbs', 'on' );

	if( $is_breadcrumbs === 'on' && !is_front_page() ) {

		$home_page_url = esc_url( home_url( '/' ) );
		$frontpage_id = get_option('page_on_front');

		$breadcrumbs = '<div class="breadcrumbs">';

		if( !$frontpage_id && is_home() ) {
			$breadcrumbs .= '<a href="'.$home_page_url.'">'.__( 'Home', 'tdmagazine' ).'</a>';
			$breadcrumbs .= '<i class="fa fa-angle-right"></i>'.__( 'All News', 'tdmagazine' );
		} else if( is_category() ) {
			$breadcrumbs .= '<a href="'.$home_page_url.'">'.__( 'Home', 'tdmagazine' ).'</a>';
			$breadcrumbs .= '<i class="fa fa-angle-right"></i>'. single_cat_title( '', false );
		} else if( is_single() ) {
			$category = get_the_category();

			$breadcrumbs .= '<a href="'.$home_page_url.'">'.__( 'Home', 'tdmagazine' ).'</a>';
			$breadcrumbs .= '<i class="fa fa-angle-right"></i><a href="'.get_category_link( $category[0]->term_id ).'">'. $category[0]->cat_name .'</a>';
			$breadcrumbs .= '<i class="fa fa-angle-right"></i>'. get_the_title();
		} else if( is_page() ) {
			$breadcrumbs .= '<a href="'.$home_page_url.'">'.__( 'Home', 'tdmagazine' ).'</a>';
			$breadcrumbs .= '<i class="fa fa-angle-right"></i>'. get_the_title();
		} else if( is_tag() ) {
			$breadcrumbs .= '<a href="'.$home_page_url.'">'.__( 'Home', 'tdmagazine' ).'</a>';
			$breadcrumbs .= '<i class="fa fa-angle-right"></i>'. single_tag_title( '', false );
		} else if( is_day() ) {
			$breadcrumbs .= '<a href="'.$home_page_url.'">'.__( 'Home', 'tdmagazine' ).'</a>';
			$breadcrumbs .= '<i class="fa fa-angle-right"></i>'. get_the_date();
		} else if( is_month() ) {
			$breadcrumbs .= '<a href="'.$home_page_url.'">'.__( 'Home', 'tdmagazine' ).'</a>';
			$breadcrumbs .= '<i class="fa fa-angle-right"></i>'. get_the_date( 'F Y' );
		} else if( is_year() ) {
			$breadcrumbs .= '<a href="'.$home_page_url.'">'.__( 'Home', 'tdmagazine' ).'</a>';
			$breadcrumbs .= '<i class="fa fa-angle-right"></i>'. get_the_date( 'Y' );
		} else if( is_author() ) {
			$breadcrumbs .= '<a href="'.$home_page_url.'">'.__( 'Home', 'tdmagazine' ).'</a>';

			if( get_query_var( 'author_name' ) )  {
				$curauth = get_user_by('slug', get_query_var('author_name'));
			} else {
				$curauth = get_userdata(get_query_var('author'));
			}

			$breadcrumbs .= '<i class="fa fa-angle-right"></i>'. $curauth->display_name;
		} else if( is_search() ) {
			$breadcrumbs .= '<a href="'.$home_page_url.'">'.__( 'Home', 'tdmagazine' ).'</a>';
			$breadcrumbs .= '<i class="fa fa-angle-right"></i>'. __( 'Search', 'tdmagazine');

			if( get_search_query() ) {
				$breadcrumbs .= '<i class="fa fa-angle-right"></i>'. get_search_query();
			}

		} else {
			$breadcrumbs .= '<a href="'.$home_page_url.'">'.__( 'Home', 'tdmagazine' ).'</a>';
			$breadcrumbs .= '<i class="fa fa-angle-right"></i>'.__( 'All News', 'tdmagazine' );
		}

		$breadcrumbs .= '</div>';

		echo $breadcrumbs;
	}
}
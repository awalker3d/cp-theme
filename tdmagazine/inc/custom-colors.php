<?php
/**
 * Custom colors
 *
 * @package tdmagazine
 */

function tdmagazine_custom_colors() {
	$custom_colors = '';

	/* Header Background Color */
	if( get_theme_mod( 'tdmagazine_header_bgcolor', '#ffffff' ) != '#ffffff' ) {
		$custom_colors .= " #masthead, #masthead .top-navigation ul {background-color: ".esc_attr( get_theme_mod( 'tdmagazine_header_bgcolor', '#ffffff' ) ).";} \n";
	}

	/* Menu Background Color */
	if( get_theme_mod( 'tdmagazine_menu_bgcolor', '#000000' ) != '#000000' ) {
		$custom_colors .= " .main-navigation, .main-navigation ul ul, #mobile-site-navigation {background: ".esc_attr( get_theme_mod( 'tdmagazine_menu_bgcolor', '#000000' ) ).";} \n";
	}

	/* Menu Text Color */
	if( get_theme_mod( 'tdmagazine_menu_textcolor', '#ffffff' ) != '#ffffff' ) {
		$custom_colors .= " #masthead .main-navigation li a, #mobile-site-navigation a {color: ".esc_attr( get_theme_mod( 'tdmagazine_menu_textcolor', '#ffffff' ) ).";} \n";
	}

	/* Footer Background Color */
	if( get_theme_mod( 'tdmagazine_footer_bgcolor', '#000000' ) != '#000000' ) {
		$custom_colors .= " #colophon {background: ".esc_attr( get_theme_mod( 'tdmagazine_footer_bgcolor', '#000000' ) ).";} \n";
	}

	/* Footer Text Color */
	if( get_theme_mod( 'tdmagazine_footer_textcolor', '#aaaaaa' ) != '#aaaaaa' ) {
		$custom_colors .= " #colophon, #colophon a:hover {color: ".esc_attr( get_theme_mod( 'tdmagazine_footer_textcolor', '#aaaaaa' ) ).";} \n";
	}

	/* Accent Color */
	if( get_theme_mod( 'tdmagazine_accent_color', '#fc4e3a' ) != '#fc4e3a' ) {
		$accent_color = get_theme_mod( 'tdmagazine_accent_color', '#fc4e3a' );
		$custom_colors .= " a, #page .accent-color { color:".esc_attr( $accent_color )."; } \n";
		$custom_colors .= " #colophon, .main-navigation .current_page_item a, .main-navigation .current-menu-item a, .main-navigation li:hover > a { border-color:".esc_attr( $accent_color )."; } \n";
		$custom_colors .= " .main-navigation ul > li.menu-item-has-children:hover > a:before { border-color: transparent transparent ".esc_attr( $accent_color )." transparent; } \n ";
		$custom_colors .= " .ticker-title span, .featured-container .bx-wrapper .bx-pager.bx-default-pager a:hover, .featured-container .bx-wrapper .bx-pager.bx-default-pager a.active, .featured-container .bx-controls-direction a:hover, .hentry .bx-controls-direction a:hover, .hentry .entry-category a { background:".esc_attr( $accent_color )."; } \n";
	}

	if( $custom_colors ):
	?>
		<style type='text/css'>
		<?php echo $custom_colors; ?>
		</style>
	<?php
	endif;
}
add_action('wp_head', 'tdmagazine_custom_colors');
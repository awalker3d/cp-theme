<?php
/**
 * Custom Widgets
 *
 * @package tdmagazine
 */

/**
 *  Recent Comments Widget
 *
 *  @since tdmagazine 1.0
 *  @updated tdmagazine 2.0
 */
class tdmagazine_recent_comments_widget extends WP_Widget {
	function __construct() {
		parent::__construct(false, $name = __( 'Recent Comments', 'tdmagazine' ), array( 'description' => __( 'This widget shows recent comments', 'tdmagazine' ) ) );
	}

	function form( $instance ) {
		$tdmagazine_widget_title = isset( $instance['tdmagazine_widget_recent_comments_widget_title'] ) ? esc_attr( $instance['tdmagazine_widget_recent_comments_widget_title'] ) : '';
		$tdmagazine_widget_number = isset( $instance['tdmagazine_widget_recent_comments_widget_number'] ) ? esc_attr( $instance['tdmagazine_widget_recent_comments_widget_number'] ) : '5';
		?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_recent_comments_widget_title' ) ); ?>"><?php _e( 'Title', 'tdmagazine' ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_recent_comments_widget_title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'tdmagazine_widget_recent_comments_widget_title' ) ); ?>" type="text" value="<?php echo esc_attr( $tdmagazine_widget_title ); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_recent_comments_widget_number' ) ); ?>"><?php _e( 'Number of comments to show:', 'tdmagazine' ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_recent_comments_widget_number' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'tdmagazine_widget_recent_comments_widget_number' ) ); ?>" type="text" value="<?php echo esc_attr( $tdmagazine_widget_number ); ?>" />
			</p>
		<?php

	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['tdmagazine_widget_recent_comments_widget_title' ] = strip_tags( $new_instance['tdmagazine_widget_recent_comments_widget_title'] );
  		$instance['tdmagazine_widget_recent_comments_widget_number' ] = intval(strip_tags( $new_instance['tdmagazine_widget_recent_comments_widget_number'] ));

    	return $instance;
	}

	function widget( $args, $instance ) {
		$recent_comments = get_comments( array(
			'number'    => $instance['tdmagazine_widget_recent_comments_widget_number'],
			'status'    => 'approve'
		) );

		$widget_title = $instance['tdmagazine_widget_recent_comments_widget_title'];

		echo $args['before_widget'];

		if( !empty( $widget_title ) ) {
			echo '<h4 class="widget-title accent-color">'.esc_html( $widget_title ).'</h4>';
		}

		if ( $recent_comments ): ?>

		<ul class="recent-comments">
		<?php foreach( $recent_comments as $comment ): ?>
			<li class="clearfix">
				<div class="author-widget-image alignleft">
				<?php echo get_avatar( $comment->comment_author_email, 92); ?>
				</div><!-- .author-widget-image -->

				<div class="recent-comment-info">
					<h5 class="author-name"><?php echo $comment->comment_author; ?></h5><!-- .author-name -->
					<a class="entry-link" href="<?php echo esc_url( get_permalink($comment->comment_post_ID).'#comment-'.$comment->comment_ID ); ?>" title="<?php echo esc_attr( $comment->comment_author .' | '.get_the_title($comment->comment_post_ID) ); ?>">
						<?php echo get_the_title( $comment->comment_post_ID ); ?>
					</a><!-- .entry-link -->
				</div><!-- .recent-comment-info -->
			</li>
		<?php endforeach; ?>
		</ul><!-- .recent-comments -->

		<?php endif;
		echo $args['after_widget'];
	}

}

/**
 * 	Authors List Widget
 *
 *	@since tdmagazine 1.0
 *  @updated tdmagazine 2.0
 */
class tdmagazine_author_widget extends WP_Widget {
	function __construct() {
		parent::__construct(false, $name = __( 'Authors Widget', 'tdmagazine' ), array( 'description' => __( 'This widget displays recent,random or most popular site authors.', 'tdmagazine' ) ) );
	}

	function form( $instance ) {
		$tdmagazine_widget_title = isset( $instance['tdmagazine_widget_author_widget_title'] ) ? esc_attr( $instance['tdmagazine_widget_author_widget_title'] ) : '';
		$tdmagazine_widget_number = isset( $instance['tdmagazine_widget_author_widget_number'] ) ? esc_attr( $instance['tdmagazine_widget_author_widget_number'] ) : '5';
		$tdmagazine_widget_sort = isset( $instance['tdmagazine_widget_author_widget_sort'] ) ? esc_attr( $instance['tdmagazine_widget_author_widget_sort'] ) : '';
		?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_author_widget_title' ) ); ?>"><?php _e( 'Title', 'tdmagazine' ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_author_widget_title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'tdmagazine_widget_author_widget_title' ) ); ?>" type="text" value="<?php echo esc_attr( $tdmagazine_widget_title ); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_author_widget_number' ) ); ?>"><?php _e( 'Number of authors to show:', 'tdmagazine' ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_author_widget_number' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'tdmagazine_widget_author_widget_number' ) ); ?>" type="text" value="<?php echo esc_attr( $tdmagazine_widget_number ); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_author_widget_sort' ) ); ?>"><?php _e( 'Sort by:', 'tdmagazine' ); ?></label>
				<select id="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_author_widget_sort' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'tdmagazine_widget_author_widget_sort' ) ); ?>">
					<option value="recent" <?php selected($tdmagazine_widget_sort, 'recent', true); ?>><?php _e( 'Recent', 'tdmagazine' ); ?></option>
				  	<option value="popular" <?php selected($tdmagazine_widget_sort, 'popular', true); ?>><?php _e( 'Popular', 'tdmagazine' ); ?></option>
				  	<option value="name" <?php selected($tdmagazine_widget_sort, 'name', true); ?>><?php _e( 'Name', 'tdmagazine' ); ?></option>
				</select>
			</p>
		<?php

	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['tdmagazine_widget_author_widget_title'] = strip_tags( $new_instance['tdmagazine_widget_author_widget_title'] );
  		$instance['tdmagazine_widget_author_widget_number'] = intval(strip_tags( $new_instance['tdmagazine_widget_author_widget_number'] ));
  		$instance['tdmagazine_widget_author_widget_sort'] = strip_tags( $new_instance['tdmagazine_widget_author_widget_sort'] );

    	return $instance;
	}

	function widget( $args, $instance ) {

		$authors_order = $instance['tdmagazine_widget_author_widget_sort'];
		$widget_title = $instance['tdmagazine_widget_author_widget_title'];

		if ( $authors_order === 'recent' ) {
			$order_authors_by = 'registered';
			$author_order = 'DESC';
		} else if ( $authors_order === 'popular' ) {
			$order_authors_by = 'post_count';
			$author_order = 'DESC';
		} else if ( $authors_order === 'name' ) {
			$order_authors_by = 'display_name';
			$author_order = 'ASC';
		}

		$tdauthors = new WP_User_Query( array(
			'orderby' => $order_authors_by,
			'number' => $instance['tdmagazine_widget_author_widget_number'],
			'order' => $author_order
		));

		$authors = $tdauthors->get_results();

		echo $args['before_widget'];

		if( !empty( $widget_title ) ) {
			echo '<h4 class="widget-title accent-color">'.esc_html( $widget_title ).'</h4>';
		}

		if ( !empty( $authors ) ): ?>

		<ul class="authors-list">
		<?php foreach( $authors as $author ): ?>
			<?php $user_post_count = count_user_posts( $author->ID ); ?>
			<li class="clearfix">
				<a class="author-widget-image alignleft" href="<?php echo esc_url( get_author_posts_url( $author->ID ) ); ?>">
				<?php echo get_avatar( $author->ID, 92 ); ?>
				</a><!-- .author-widget-image -->
				<h5 class="author-widget-name">
					<a href="<?php echo esc_url( get_author_posts_url( $author->ID ) ); ?>" title="<?php echo esc_attr( __( 'View all posts by', 'tdmagazine' ) . ' '. $author->display_name ); ?>"><?php echo $author->display_name; ?></a>
				</h5><!-- .author-widget-name -->
				<div class="author-post-count">
				<?php if( $user_post_count > 1 || $user_post_count >= 0 ): ?>
					<?php echo $user_post_count . ' ' . __( 'posts', 'tdmagazine' ); ?>
				<?php else: ?>
					<?php echo $user_post_count . ' ' . __( 'post', 'tdmagazine' ); ?>
				<?php endif; ?>
				</div><!-- .author-post-count -->
			</li><!-- .clearfix -->
		<?php endforeach; ?>
		</ul><!-- .authors-list -->

		<?php endif;
		wp_reset_query();
		echo $args['after_widget'];
	}

}

/**
 *  Login Widget
 *
 *	@since tdmagazine 1.0
 *  @updated tdmagazine 2.0
 */
class tdmagazine_login_widget extends WP_Widget {
	function __construct() {
		parent::__construct(false, $name = __( 'Login Widget', 'tdmagazine' ), array( 'description' => __( 'This widget allows you to add a login form.', 'tdmagazine' ) ) );
	}

	function form( $instance ) {
		$tdmagazine_widget_title = isset( $instance['tdmagazine_widget_login_widget_title'] ) ? esc_attr( $instance['tdmagazine_widget_login_widget_title'] ) : '';

		?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_login_widget_title' ) ); ?>"><?php _e( 'Title', 'tdmagazine' ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_login_widget_title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'tdmagazine_widget_login_widget_title' ) ); ?>" type="text" value="<?php echo esc_attr( $tdmagazine_widget_title ); ?>" />
			</p>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['tdmagazine_widget_login_widget_title' ] = strip_tags( $new_instance['tdmagazine_widget_login_widget_title'] );

    	return $instance;
	}

	function widget( $args, $instance ) {
		$widget_title = $instance['tdmagazine_widget_login_widget_title'];
		echo $args['before_widget'];

		if( !empty( $widget_title ) ){
			echo '<h4 class="widget-title accent-color">'.esc_html( $widget_title ).'</h4>';
		}

		if( !is_user_logged_in() ):
			wp_login_form(); ?>
			<div class="lost-password-container">
				<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" title="<?php esc_attr_e( 'Lost Password', 'tdmagazine' ); ?>">
					<?php _e( 'Lost Password ?', 'tdmagazine' ); ?>
				</a>
			</div><!-- .lost-password-container -->

		<?php
			else:
			$current_user = wp_get_current_user();
			$user_info = get_userdata( $current_user->ID );
		?>
			<div class="profile-picture-name">
				<?php echo get_avatar( $current_user->user_email, 96); ?>
			</div><!-- .profile-picture-name -->
			<h5 class="user-name"><?php echo $current_user->display_name; ?></h5>
			<?php if( !empty( $user_info->description ) ):
				printf( '<div class="user-bio">%s</div>', $user_info->description );
			endif; ?>
			<a class="logout-link" href="<?php echo esc_url( wp_logout_url( home_url() ) ); ?>" title="<?php esc_attr_e( 'Logout', 'tdmagazine' ); ?>"><?php _e( 'Logout', 'tdmagazine' ); ?></a>
		<?php endif;
		echo $args['after_widget'];
	}

}

/**
 *  Newsletter Widget
 *	@since tdmagazine 1.0
 *  @updated tdmagazine 2.0
 */
class tdmagazine_newsletter_widget extends WP_Widget {
	function __construct() {
		parent::__construct(false, $name = __( 'Newsletter Widget', 'tdmagazine' ), array( 'description' => __( 'This widget allows you to add your own newsletter form.', 'tdmagazine' ) ) );
	}

	function form( $instance ) {
		$tdmagazine_widget_title = isset( $instance['tdmagazine_widget_newsletter_widget_title'] ) ? esc_attr( $instance['tdmagazine_widget_newsletter_widget_title'] ) : '';

		?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_newsletter_widget_title' ) ); ?>"><?php _e( 'Title', 'tdmagazine' ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'tdmagazine_widget_newsletter_widget_title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'tdmagazine_widget_newsletter_widget_title' ) ); ?>" type="text" value="<?php echo esc_attr( $tdmagazine_widget_title ); ?>" />
			</p>
		<?php

	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['tdmagazine_widget_newsletter_widget_title' ] = strip_tags( $new_instance['tdmagazine_widget_newsletter_widget_title'] );

    	return $instance;
	}

	function widget( $args, $instance ) {
		echo $args['before_widget'];
		$widget_title = $instance['tdmagazine_widget_newsletter_widget_title'];

		if( !empty( $widget_title ) ){
			echo '<h4 class="widget-title accent-color">'.esc_html( $widget_title ).'</h4>';
		} ?>

		<?php if( tdmagazine_newsletter_form() ): ?>
			<div class="newsletter-container">
				<?php echo tdmagazine_newsletter_image(); ?>
				<div class="newsletter-inner">
					<?php
						$tdmagazine_newsletter = tdmagazine_newsletter_form();
						echo $tdmagazine_newsletter['newsletter-code'];
					?>
				</div><!-- .newsletter-inner -->
			</div><!-- .newsletter-container -->
		<?php endif; ?>

		<?php
		echo $args['after_widget'];
	}
}

/**
 *  Recent Posts Widget (Standard)
 *
 *	@since tdmagazine 2.0
 */
class tdmagazine_rp_standard_widget extends WP_Widget {
	function __construct() {
		parent::__construct(false, $name = __( 'Recent Posts (Standard)', 'tdmagazine' ), array( 'description' => __( 'This widget allows you to display recent posts from a specific category.', 'tdmagazine' ) ) );
	}

	function form( $instance ) {
		$tdmagazine_widget_title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$tdmagazine_widget_category = isset( $instance['category'] ) ? esc_attr( $instance['category'] ) : '';
		$tdmagazine_widget_per_page = isset( $instance['per_page'] ) ? esc_attr( $instance['per_page'] ) : '5';
		$tdmagazine_widget_order = isset( $instance['order'] ) ? esc_attr( $instance['order'] ) : '';

		?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'tdmagazine' ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $tdmagazine_widget_title ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"><?php _e( 'Category:', 'tdmagazine' ); ?></label><br>
				<?php
					wp_dropdown_categories( array(
							'name'              => $this->get_field_name( 'category' ),
							'echo'              => true,
							'show_option_none'   => __( 'None', 'tdmagazine' ),
							'id'				=> $this->get_field_id( 'category' ),
							'selected'          => $tdmagazine_widget_category,
					) );
				?>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>"><?php _e( 'Order:', 'tdmagazine' ); ?></label>
				<select id="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'order' ) ); ?>">
					<option value="" <?php selected( $tdmagazine_widget_order, '' ); ?>><?php _e( 'Recent', 'tdmagazine' ); ?></option>
				  	<option value="popular" <?php selected( $tdmagazine_widget_order, 'popular' ); ?>><?php _e( 'Popular', 'tdmagazine' ); ?></option>
				  	<option value="random" <?php selected( $tdmagazine_widget_order, 'random' ); ?>><?php _e( 'Random', 'tdmagazine' ); ?></option>
				</select>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'per_page' ) ); ?>"><?php _e( 'Posts per category:', 'tdmagazine' ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'per_page' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'per_page' ) ); ?>" type="number" value="<?php echo esc_attr( $tdmagazine_widget_per_page ); ?>" />
			</p>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['category'] = intval( $new_instance['category'] );
		$instance['per_page'] = intval( $new_instance['per_page'] );
		$instance['order'] = strip_tags( $new_instance['order'] );
    	return $instance;
	}

	function widget( $args, $instance ) {
		$widget_title = $instance['title'];
		$selected_category = $instance['category'];
		$posts_per_category = $instance['per_page'];
		$order = isset( $instance['order'] ) ? $instance['order'] : '';

		if( $selected_category <= 0 ) {
			if( $order === 'popular' ) {
				$current_category_args = array(
					'post_type' => 'post',
					'posts_per_page' => $posts_per_category,
					'ignore_sticky_posts' => 1,
					'orderby'=>'comment_count',
					'order' => 'DESC'
				);
			} else if( $order === 'random'  ) {
				$current_category_args = array(
					'post_type' => 'post',
					'posts_per_page' => $posts_per_category,
					'ignore_sticky_posts' => 1,
					'orderby'=>'rand'
				);
			} else {
				$current_category_args = array(
					'post_type' => 'post',
					'posts_per_page' => $posts_per_category,
					'ignore_sticky_posts' => 1
				);
			}
		} else {
			if( $order === 'popular' ) {
				$current_category_args = array(
					'post_type' => 'post',
					'category__in' => $selected_category,
					'posts_per_page' => $posts_per_category,
					'ignore_sticky_posts' => 1,
					'orderby'=>'comment_count',
					'order' => 'DESC'
				);
			} else if( $order === 'random'  ) {
				$current_category_args = array(
					'post_type' => 'post',
					'category__in' => $selected_category,
					'posts_per_page' => $posts_per_category,
					'ignore_sticky_posts' => 1,
					'orderby'=>'rand'
				);
			} else {
				$current_category_args = array(
					'post_type' => 'post',
					'category__in' => $selected_category,
					'posts_per_page' => $posts_per_category,
					'ignore_sticky_posts' => 1
				);
			}
		}


		$count = 0;
		$category_items_query = new WP_Query( $current_category_args );
		echo $args['before_widget'];

		if( !empty( $widget_title ) ): ?>

			<?php if( $selected_category <= 0 ): ?>
				<h4 class="widget-title">
				<a class="cat-link accent-color" href="<?php echo esc_url( get_permalink( get_option( 'page_for_posts' ) ) ); ?>"><?php echo esc_html( $widget_title ); ?></a>
				</h4><!-- .widget-title -->
			<?php else: ?>
				<h4 class="widget-title">
				<a class="cat-link accent-color" href="<?php echo esc_url( get_category_link( $selected_category ) ); ?>"><?php echo esc_html( $widget_title ); ?></a>
				</h4><!-- .widget-title -->
			<?php endif; ?>

		<?php else: ?>

			<?php if( $selected_category <= 0 ): ?>
				<h4 class="widget-title">
				<a class="cat-link accent-color" href="<?php echo esc_url( get_permalink( get_option( 'page_for_posts' ) ) ); ?>"><?php _e( 'Recent Posts', 'tdmagazine' ); ?></a>
				</h4><!-- .widget-title -->
			<?php else: ?>
				<h4 class="widget-title">
				<a class="cat-link accent-color" href="<?php echo esc_url( get_category_link( $selected_category ) ); ?>"><?php echo get_cat_name( $selected_category ); ?></a>
				</h4><!-- .widget-title -->
			<?php endif; ?>

		<?php endif; ?>

		<?php if( $category_items_query->have_posts() ): ?>
			<div class="row standard-layout">
			<?php while( $category_items_query->have_posts() ): $category_items_query->the_post(); ?>
				<?php if( $count <= 0 ): ?>
				<div class="col-lg-12 recent-item">
					<?php get_template_part( 'parts/home-content', 'standard-latest' ); ?>
				</div><!-- .recent-item -->
				<?php else: ?>

				<?php if( $count % 2 == 1 ): ?>
				</div><!-- .row -->
				<div class="row other-recent-items">
				<?php endif; ?>

				<div class="col-lg-6 col-md-6 recent-item">
					<?php get_template_part( 'parts/home-content', 'standard' ); ?>
				</div><!-- .recent-item -->
				<?php endif; ?>

				<?php $count++; ?>
			<?php endwhile; ?>
			</div><!-- .row -->
		<?php endif; ?>

		<?php
		echo $args['after_widget'];
		wp_reset_postdata();
	}
}

/**
 *  Recent Posts Widget (Columns)
 *
 *	@since tdmagazine 2.0
 */
class tdmagazine_rp_columns_widget extends WP_Widget {
	function __construct() {
		parent::__construct(false, $name = __( 'Recent Posts (Columns)', 'tdmagazine' ), array( 'description' => __( 'This widget allows you to display recent posts from a specific category.', 'tdmagazine' ) ) );
	}

	function form( $instance ) {
		$tdmagazine_widget_title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$tdmagazine_widget_category = isset( $instance['category'] ) ? esc_attr( $instance['category'] ) : '';
		$tdmagazine_widget_order = isset( $instance['order'] ) ? esc_attr( $instance['order'] ) : '';
		$tdmagazine_widget_per_page = isset( $instance['per_page'] ) ? esc_attr( $instance['per_page'] ) : '4';
		$tdmagazine_widget_layout = isset( $instance['section_layout'] ) ? esc_attr( $instance['section_layout'] ) : 'one';

		?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'tdmagazine' ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $tdmagazine_widget_title ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"><?php _e( 'Category:', 'tdmagazine' ); ?></label><br>
				<?php
					wp_dropdown_categories( array(
							'name'              => $this->get_field_name( 'category' ),
							'echo'              => true,
							'show_option_none'   => __( 'None', 'tdmagazine' ),
							'id'				=> $this->get_field_id( 'category' ),
							'selected'          => $tdmagazine_widget_category,
					) );
				?>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>"><?php _e( 'Order:', 'tdmagazine' ); ?></label>
				<select id="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'order' ) ); ?>">
					<option value="" <?php selected( $tdmagazine_widget_order, '' ); ?>><?php _e( 'Recent', 'tdmagazine' ); ?></option>
				  	<option value="popular" <?php selected( $tdmagazine_widget_order, 'popular' ); ?>><?php _e( 'Popular', 'tdmagazine' ); ?></option>
				  	<option value="random" <?php selected( $tdmagazine_widget_order, 'random' ); ?>><?php _e( 'Random', 'tdmagazine' ); ?></option>
				</select>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'per_page' ) ); ?>"><?php _e( 'Posts per category:', 'tdmagazine' ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'per_page' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'per_page' ) ); ?>" type="number" value="<?php echo esc_attr( $tdmagazine_widget_per_page ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'section_layout' ) ); ?>"><?php _e( 'Layout:', 'tdmagazine' ); ?></label>
				<select id="<?php echo esc_attr( $this->get_field_id( 'section_layout' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'section_layout' ) ); ?>">
					<option value="one" <?php selected( $tdmagazine_widget_layout, 'one' ); ?>><?php _e( 'One Column', 'tdmagazine' ); ?></option>
				  	<option value="two" <?php selected( $tdmagazine_widget_layout, 'two' ); ?>><?php _e( 'Two Columns', 'tdmagazine' ); ?></option>
				</select>
			</p>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['category'] = intval( $new_instance['category'] );
		$instance['per_page'] = intval( $new_instance['per_page'] );
		$instance['order'] = strip_tags( $new_instance['order'] );
		$instance['section_layout'] = strip_tags( $new_instance['section_layout'] );
    	return $instance;
	}

	function widget( $args, $instance ) {
		$widget_title = $instance['title'];
		$selected_category = $instance['category'];
		$posts_per_category = $instance['per_page'];
		$layout = $instance['section_layout'];
		$order = $instance['order'];

		if( $selected_category <= 0 ) {
			if( $order === 'popular' ) {
				$current_category_args = array(
					'post_type' => 'post',
					'posts_per_page' => $posts_per_category,
					'ignore_sticky_posts' => 1,
					'orderby'=>'comment_count',
					'order' => 'DESC'
				);
			} else if( $order === 'random'  ) {
				$current_category_args = array(
					'post_type' => 'post',
					'posts_per_page' => $posts_per_category,
					'ignore_sticky_posts' => 1,
					'orderby'=>'rand'
				);
			} else {
				$current_category_args = array(
					'post_type' => 'post',
					'posts_per_page' => $posts_per_category,
					'ignore_sticky_posts' => 1
				);
			}
		} else {
			if( $order === 'popular' ) {
				$current_category_args = array(
					'post_type' => 'post',
					'category__in' => $selected_category,
					'posts_per_page' => $posts_per_category,
					'ignore_sticky_posts' => 1,
					'orderby'=>'comment_count',
					'order' => 'DESC'
				);
			} else if( $order === 'random'  ) {
				$current_category_args = array(
					'post_type' => 'post',
					'category__in' => $selected_category,
					'posts_per_page' => $posts_per_category,
					'ignore_sticky_posts' => 1,
					'orderby'=>'rand'
				);
			} else {
				$current_category_args = array(
					'post_type' => 'post',
					'category__in' => $selected_category,
					'posts_per_page' => $posts_per_category,
					'ignore_sticky_posts' => 1
				);
			}
		}

		$category_items_query = new WP_Query( $current_category_args );
		echo $args['before_widget'];

		if( !empty( $widget_title ) ): ?>

			<?php if( $selected_category < 0 ): ?>
				<h4 class="widget-title">
				<a class="cat-link accent-color" href="<?php echo esc_url( get_permalink( get_option( 'page_for_posts' ) ) ); ?>"><?php echo esc_html( $widget_title ); ?></a>
				</h4><!-- .widget-title -->
			<?php else: ?>
				<h4 class="widget-title">
				<a class="cat-link accent-color" href="<?php echo esc_url( get_category_link( $selected_category ) ); ?>"><?php echo esc_html( $widget_title ); ?></a>
				</h4><!-- .widget-title -->
			<?php endif; ?>

		<?php else: ?>

			<?php if( $selected_category < 0 ): ?>
				<h4 class="widget-title">
				<a class="cat-link accent-color" href="<?php echo esc_url( get_permalink( get_option( 'page_for_posts' ) ) ); ?>"><?php _e( 'Recent Posts', 'tdmagazine' ); ?></a>
				</h4><!-- .widget-title -->
			<?php else: ?>
				<h4 class="widget-title">
				<a class="cat-link accent-color" href="<?php echo esc_url( get_category_link( $selected_category ) ); ?>"><?php echo get_cat_name( $selected_category ); ?></a>
				</h4><!-- .widget-title -->
			<?php endif; ?>

		<?php endif; ?>

		<?php if( $category_items_query->have_posts() ): ?>

			<div class="row columns-layout content-grid">
				<?php if( $layout === 'two' ): ?>

				<?php while( $category_items_query->have_posts() ): $category_items_query->the_post(); ?>
					<div class="col-lg-6 col-md-6 recent-item two-columns post-box">
					<?php get_template_part( 'parts/home-content', 'columns' ); ?>
					</div><!-- .two-columns -->
				<?php endwhile; ?>

				<?php else: ?>

				<?php while( $category_items_query->have_posts() ): $category_items_query->the_post(); ?>
					<div class="col-lg-12 recent-item one-column post-box">
					<?php get_template_part( 'parts/home-content', 'columns' ); ?>
					</div><!-- .one-column -->
				<?php endwhile; ?>

				<?php endif; ?>
			</div><!-- .row -->

		<?php endif; ?>

		<?php
		echo $args['after_widget'];
		wp_reset_postdata();
	}
}
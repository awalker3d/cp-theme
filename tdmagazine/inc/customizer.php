<?php
/**
 * tdMagazine Theme Customizer
 *
 * @package tdMagazine
 */
function tdmagazine_customize_register( $wp_customize ) {
	$wp_customize->remove_control('header_image');

	/* Header Setting */
	$wp_customize->add_section( 'tdmagazine_header_section', array(
    	'title'          => __('Header Settings', 'tdmagazine'),
    	'priority'       => 1,
	) );

	/* Site Logo */
	$wp_customize->add_setting( 'tdmagazine_header_logo_img',array(
		'default' => '',
        'sanitize_callback' => 'esc_url_raw'
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'tdmagazine_header_logo_img', array(
		'label' => __( 'Custom Logo Upload', 'tdmagazine' ),
		'section' => 'tdmagazine_header_section',
		'settings' => 'tdmagazine_header_logo_img'
	) ) );

	/* Custom Favicon */
	$wp_customize->add_setting( 'tdmagazine_header_favicon_img', array(
		'default' => '',
		'sanitize_callback' => 'tdmagazine_header_favicon_sanitize'
	) );

	$wp_customize->add_control( new WP_Customize_Upload_Control( $wp_customize, 'tdmagazine_header_favicon_img', array(
		'label' => __( 'Custom Favicon Upload', 'tdmagazine' ),
		'section' => 'tdmagazine_header_section',
		'settings' => 'tdmagazine_header_favicon_img',
	) ) );

	//********************
	//	Colors
	//********************

	/* Header Background Color*/
	$wp_customize->add_setting( 'tdmagazine_header_bgcolor', array(
    	'default'        => '#ffffff',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tdmagazine_header_bgcolor', array(
		'label' => __( 'Header Background Color', 'tdmagazine' ),
		'section' => 'colors',
		'settings' => 'tdmagazine_header_bgcolor',
		'priority' => 1
    ) ) );

	/* Menu Background Color */
	$wp_customize->add_setting( 'tdmagazine_menu_bgcolor', array(
    	'default'        => '#000000',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tdmagazine_menu_bgcolor', array(
		'label' => __( 'Menu Background Color', 'tdmagazine' ),
		'section' => 'colors',
		'settings' => 'tdmagazine_menu_bgcolor',
		'priority' => 100
    ) ) );

    /* Menu Text Color */
	$wp_customize->add_setting( 'tdmagazine_menu_textcolor', array(
    	'default'        => '#ffffff',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tdmagazine_menu_textcolor', array(
		'label' => __( 'Menu Text Color', 'tdmagazine' ),
		'section' => 'colors',
		'settings' => 'tdmagazine_menu_textcolor',
		'priority' => 101
    ) ) );

    /* Accent Color */
	$wp_customize->add_setting( 'tdmagazine_accent_color', array(
    	'default'        => '#fc4e3a',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tdmagazine_accent_color', array(
		'label' => __( 'Accent Color', 'tdmagazine' ),
		'section' => 'colors',
		'settings' => 'tdmagazine_accent_color',
		'priority' => 102
    ) ) );

    /* Footer Background Color */
	$wp_customize->add_setting( 'tdmagazine_footer_bgcolor', array(
    	'default'        => '#000000',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tdmagazine_footer_bgcolor', array(
		'label' => __( 'Footer Background Color', 'tdmagazine' ),
		'section' => 'colors',
		'settings' => 'tdmagazine_footer_bgcolor',
		'priority' => 200
    ) ) );

    /* Footer Text Color */
	$wp_customize->add_setting( 'tdmagazine_footer_textcolor', array(
    	'default'        => '#aaaaaa',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tdmagazine_footer_textcolor', array(
		'label' => __( 'Footer Background Color', 'tdmagazine' ),
		'section' => 'colors',
		'settings' => 'tdmagazine_footer_textcolor',
		'priority' => 200
    ) ) );

 	//********************
	//	Sidebar Options
	//********************
 	$wp_customize->add_section( 'tdmagazine_sidebar_settings', array(
     	'title'    => __( 'Sidebar Options', 'tdmagazine' ),
     	'priority' => 4999,
	) );

    /* Posts Page Sidebar */
	$wp_customize->add_setting( 'tdmagazine_blog_sidebar', array(
        'default' => '1',
        'sanitize_callback' => 'tdmagazine_sanitize_number'
    ) );

	$wp_customize->add_control( 'tdmagazine_blog_sidebar', array(
        'type' => 'select',
        'label' => __( 'Posts Page Sidebar', 'tdmagazine' ),
        'section' => 'tdmagazine_sidebar_settings',
        'choices' => array(
            '1' => __( 'Auto', 'tdmagazine' ),
            '0' => __( 'Disable', 'tdmagazine' ),
        ),
        'priority' => 1
    ));

    /* Single Post Sidebar */
	$wp_customize->add_setting( 'tdmagazine_single_post_sidebar', array(
        'default' => '1',
        'sanitize_callback' => 'tdmagazine_sanitize_number'
    ) );

	$wp_customize->add_control( 'tdmagazine_single_post_sidebar', array(
        'type' => 'select',
        'label' => __( 'Single Post Sidebar', 'tdmagazine' ),
        'section' => 'tdmagazine_sidebar_settings',
        'choices' => array(
            '1' => __( 'Auto', 'tdmagazine' ),
            '0' => __( 'Disable', 'tdmagazine' ),
        ),
        'priority' => 2
    ));

	//********************
	//	Social Links Section
	//********************
	$wp_customize->add_section( 'tdmagazine_social_section', array(
    	'title'          => __( 'Social Links Options', 'tdmagazine' ),
    	'priority'       => 5000,
    	'description'	=> __( 'Select  a section where you want to display your social links.', 'tdmagazine' )
	) );

	/* Header Social Links */
    $wp_customize->add_setting( 'tdmagazine_social_top', array(
    	'default'        => 'off',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'tdmagazine_social_top', array(
        'type' => 'select',
        'label' => __( 'Header Social Links', 'tdmagazine' ),
        'section' => 'tdmagazine_social_section',
        'choices' => array(
            'on' => __( 'On', 'tdmagazine' ),
            'off' => __( 'Off', 'tdmagazine' )
        ),
        'priority'       => 1,
    ));

    /* Footer Social Links */
    $wp_customize->add_setting( 'tdmagazine_social_bottom', array(
    	'default'        => 'on',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'tdmagazine_social_bottom', array(
        'type' => 'select',
        'label' => __( 'Footer Social Links', 'tdmagazine' ),
        'section' => 'tdmagazine_social_section',
        'choices' => array(
            'on' => __( 'On', 'tdmagazine' ),
            'off' => __( 'Off', 'tdmagazine' )
        ),
        'priority'       => 2,
    ));

    //********************
	//	Website Options
	//********************
	$wp_customize->add_section( 'tdmagazine_website_settings', array(
     	'title'    => __( 'Website Options', 'tdmagazine' ),
     	'priority' => 2000,
	) );

	/* Site width */
    $wp_customize->add_setting( 'tdmagazine_website_width', array(
    	'default'        => '',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'tdmagazine_website_width', array(
        'type' => 'select',
        'label' => __( 'Site Width', 'tdmagazine' ),
        'section' => 'tdmagazine_website_settings',
        'choices' => array(
        	'' => __( 'Wide', 'tdmagazine' ),
            'narrow' => __( 'Narrow', 'tdmagazine' ),
        ),
        'priority' => 1
    ));

	/* Blog Layout */
    $wp_customize->add_setting( 'tdmagazine_website_settings_blog_style', array(
    	'default'        => 'one-col',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'tdmagazine_website_settings_blog_style', array(
        'type' => 'select',
        'label' => __( 'Blog Layout', 'tdmagazine' ),
        'section' => 'tdmagazine_website_settings',
        'choices' => array(
        	'classic' => __( 'One Column', 'tdmagazine' ),
            'two-col' => __( 'Two Columns', 'tdmagazine' ),
            'three-col' => __( 'Three Columns', 'tdmagazine' ),
            'inline' => __( 'Inline', 'tdmagazine' )
        ),
        'priority' => 2
    ));

    /* Breadcrumbs */
    $wp_customize->add_setting( 'tdmagazine_website_settings_breadcrumbs', array(
    	'default'        => 'on',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'tdmagazine_website_settings_breadcrumbs', array(
        'type' => 'select',
        'label' => __( 'Breadcrumbs', 'tdmagazine' ),
        'section' => 'tdmagazine_website_settings',
        'choices' => array(
            'on' => __( 'On', 'tdmagazine' ),
            'off' => __( 'Off', 'tdmagazine' )
        ),
        'priority' => 3
    ));

    /* Auto Summary */
    $wp_customize->add_setting( 'tdmagazine_website_settings_auto_summary', array(
    	'default'        => 'off',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'tdmagazine_website_settings_auto_summary', array(
        'type' => 'select',
        'label' => __( 'Auto Post Summary', 'tdmagazine' ),
        'section' => 'tdmagazine_website_settings',
        'choices' => array(
            'on' => __( 'On', 'tdmagazine' ),
            'off' => __( 'Off', 'tdmagazine' )
        ),
        'priority'       => 5,
    ));

    /* Auto Summmary Length */
	$wp_customize->add_setting( 'tdmagazine_website_settings_auto_summary_length', array(
    	'default'        => '55',
    	'sanitize_callback' => 'tdmagazine_sanitize_number'
	) );

	$wp_customize->add_control( new tdmagazine_Customize_Number_Control( $wp_customize, 'tdmagazine_website_settings_auto_summary_length', array(
		'type' => 'number',
		'label'   =>  __( 'Post Summary Length', 'tdmagazine' ),
		'section' => 'tdmagazine_website_settings',
		'settings'   => 'tdmagazine_website_settings_auto_summary_length',
		'priority' => 6
	) ) );

	/* Author Section */
    $wp_customize->add_setting( 'tdmagazine_website_settings_author_section', array(
    	'default'        => 'on',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'tdmagazine_website_settings_author_section', array(
        'type' => 'select',
        'label' => __( 'Author Section', 'tdmagazine' ),
        'section' => 'tdmagazine_website_settings',
        'choices' => array(
            'on' => __( 'On', 'tdmagazine' ),
            'off' => __( 'Off', 'tdmagazine' )
        ),
        'priority' => 7
    ));

    /* Related Posts Section */
    $wp_customize->add_setting( 'tdmagazine_website_settings_related_posts_section', array(
    	'default'        => 'on',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'tdmagazine_website_settings_related_posts_section', array(
        'type' => 'select',
        'label' => __( 'Related Posts', 'tdmagazine' ),
        'section' => 'tdmagazine_website_settings',
        'choices' => array(
            'on' => __( 'On', 'tdmagazine' ),
            'off' => __( 'Off', 'tdmagazine' )
        ),
        'priority' => 8
    ));

    /* Numeric Navigation */
	$wp_customize->add_setting( 'tdmagazine_numeric_navigation', array(
        'default' => '0',
        'sanitize_callback' => 'tdmagazine_sanitize_number'
    ) );

	$wp_customize->add_control( 'tdmagazine_numeric_navigation', array(
        'type' => 'select',
        'label' => __( 'Numeric Navigation', 'tdmagazine' ),
        'section' => 'tdmagazine_website_settings',
        'choices' => array(
            '1' => __( 'On', 'tdmagazine' ),
            '0' => __( 'Off', 'tdmagazine' ),
        ),
        'priority' => 4
    ));

    /* number of author per page */
	$wp_customize->add_setting( 'tdmagazine_website_settings_authors_perpage', array(
    	'default'        => '10',
    	'sanitize_callback' => 'tdmagazine_sanitize_number'
	) );

	$wp_customize->add_control( 'tdmagazine_website_settings_authors_perpage', array(
        'label' => __( 'Number of authors per page', 'tdmagazine' ),
        'section' => 'tdmagazine_website_settings',
        'type' => 'text',
        'priority'       => 1001,
    ));

    //********************
	//	Breaking News Options
	//********************
	$wp_customize->add_section( 'tdmagazine_breakingnews_settings', array(
     	'title'    => __( 'Breaking News Options', 'tdmagazine' ),
     	'priority' => 2001,
	) );

	/* Breaking News Section */
    $wp_customize->add_setting( 'tdmagazine_website_settings_breaking_news_section', array(
    	'default'        => 'on',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'tdmagazine_website_settings_breaking_news_section', array(
        'type' => 'select',
        'label' => __( 'Breaking News', 'tdmagazine' ),
        'section' => 'tdmagazine_breakingnews_settings',
        'choices' => array(
            'on' => __( 'On', 'tdmagazine' ),
            'off' => __( 'Off', 'tdmagazine' )
        ),
        'priority' => 1
    ));

    /* Number of Breaking News Items */
	$wp_customize->add_setting( 'tdmagazine_website_breaking_news_number', array(
    	'default'        => '5',
    	'sanitize_callback' => 'tdmagazine_sanitize_number'
	) );

	$wp_customize->add_control( 'tdmagazine_website_breaking_news_number', array(
		'label' => __( 'Number of Breaking News to show', 'tdmagazine' ),
		'section' => 'tdmagazine_breakingnews_settings',
		'type' => 'text',
		'priority'       => 2,
	) );

	//********************
	//	Featured Content Options
	//********************
	$wp_customize->add_section( 'tdmagazine_featured_content_settings', array(
    	'title'          => __( 'Featured Content Settings', 'tdmagazine' ),
    	'priority'       => 2002,
	) );

	/* Number of featured news */
	$wp_customize->add_setting( 'tdmagazine_home_page_featured_number', array(
    	'default'        => '5',
    	'sanitize_callback' => 'tdmagazine_sanitize_number'
	) );

	$wp_customize->add_control( 'tdmagazine_home_page_featured_number', array(
		'label' => __( 'Number of Featured Posts to show:', 'tdmagazine' ),
		'section' => 'tdmagazine_featured_content_settings',
		'type' => 'text',
		'priority' => 1
	) );

	$wp_customize->add_setting( 'tdmagazine_home_page_featured_speed', array(
    	'default'        => '7000',
    	'sanitize_callback' => 'tdmagazine_sanitize_number'
	) );

	$wp_customize->add_control( 'tdmagazine_home_page_featured_speed', array(
		'label' => __( 'Homepage Slideshow speed (milliseconds):', 'tdmagazine' ),
		'section' => 'tdmagazine_featured_content_settings',
		'type' => 'text',
		'priority' => 2
	) );

	//********************
	//	Share Buttons Options
	//********************
	$wp_customize->add_section( 'tdmagazine_share_buttons_settings', array(
     	'title'    => __( 'Share Buttons Options', 'tdmagazine' ),
     	'priority' => 2003,
	) );

	/* Share Buttons Style */
    $wp_customize->add_setting( 'tdmagazine_website_settings_share_buttons_style', array(
    	'default'        => 'default',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'tdmagazine_website_settings_share_buttons_style', array(
        'type' => 'select',
        'label' => __( 'Share Buttons Style', 'tdmagazine' ),
        'section' => 'tdmagazine_share_buttons_settings',
        'choices' => array(
            'default' => __( 'Default', 'tdmagazine' ),
            'custom' => __( 'Custom', 'tdmagazine' )
        ),
        'priority'       => 1,
    ));

    /* Share Buttons Top */
    $wp_customize->add_setting( 'tdmagazine_website_settings_share_buttons_top', array(
    	'default'        => 'off',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'tdmagazine_website_settings_share_buttons_top', array(
        'type' => 'select',
        'label' => __( 'Share Buttons (Top)', 'tdmagazine' ),
        'section' => 'tdmagazine_share_buttons_settings',
        'choices' => array(
            'on' => __( 'On', 'tdmagazine' ),
            'off' => __( 'Off', 'tdmagazine' )
        ),
         'priority'       => 2,
    ));

     /* Share Buttons Bottom */
    $wp_customize->add_setting( 'tdmagazine_website_settings_share_buttons_bottom', array(
    	'default'        => 'on',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'tdmagazine_website_settings_share_buttons_bottom', array(
        'type' => 'select',
        'label' => __( 'Share Buttons (Bottom)', 'tdmagazine' ),
        'section' => 'tdmagazine_share_buttons_settings',
        'choices' => array(
            'on' => __( 'On', 'tdmagazine' ),
            'off' => __( 'Off', 'tdmagazine' )
        ),
         'priority'       => 3,
    ));

     /* Share buttons title */
	$wp_customize->add_setting( 'tdmagazine_share_button_title', array(
    	'default'        => 'Share:',
    	'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'tdmagazine_share_button_title', array(
		'label' => __( 'Title', 'tdmagazine' ),
		'section' => 'tdmagazine_share_buttons_settings',
		'type' => 'text',
		'priority' => 4
	) );

































     /* Fixed Navigation */
    $wp_customize->add_setting( 'tdmagazine_nav_fixed_menu', array(
    	'default'        => 'off'
	) );

	$wp_customize->add_control( 'tdmagazine_nav_fixed_menu', array(
        'type' => 'select',
        'label' => __( 'Fixed Navigation Menu', 'tdmagazine' ),
        'section' => 'nav',
        'choices' => array(
            'on' => __( 'On', 'tdmagazine' ),
            'off' => __( 'Off', 'tdmagazine' )
        ),
    ));

    /* Newsletter Section */
	$wp_customize->add_section( 'tdmagazine_newsletter_section', array(
    	'title'          => __( 'Newsletter Settings', 'tdmagazine' ),
    	'priority'       => 1001,
	) );

	/* custom newsletter image */
	$wp_customize->add_setting( 'tdmagazine_newsletter_image' );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'tdmagazine_newsletter_image', array(
		'label' => __('Custom Image Upload', 'tdmagazine'),
		'section' => 'tdmagazine_newsletter_section',
		'settings' => 'tdmagazine_newsletter_image'
	) ) );

	/* newsletter form */
	$wp_customize->add_setting( 'tdmagazine_newsletter_form', array(
    	'default'        => '',
	) );

	$wp_customize->add_control( new tdmagazine_Customize_Textarea_Control( $wp_customize, 'tdmagazine_newsletter_form', array(
		'label'   => __( 'Newsletter Form(HTML Code)', 'tdmagazine' ),
		'section' => 'tdmagazine_newsletter_section',
		'settings'   => 'tdmagazine_newsletter_form',
	) ) );

	/* Background Section */
	$wp_customize->add_section( 'tdmagazine_background_section', array(
    	'title'          => __( 'Background Settings', 'tdmagazine' ),
    	'priority'       => 1000,
	) );

	/* background color */
	$wp_customize->add_setting( 'tdmagazine_background_color', array(
    	'default'        => '#e9e9e9'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tdmagazine_background_color', array(
		'label' => __( 'Background Color', 'tdmagazine' ),
		'section' => 'tdmagazine_background_section',
		'settings' => 'tdmagazine_background_color',
		'priority' => 1
    ) ) );

    /* background pattern */
    $wp_customize->add_setting( 'tdmagazine_background_pattern' );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'tdmagazine_background_pattern', array(
		'label' => __( 'Background Pattern', 'tdmagazine' ),
		'section' => 'tdmagazine_background_section',
		'settings' => 'tdmagazine_background_pattern'
	) ) );

	$wp_customize->add_setting( 'tdmagazine_background_images', array(
    	'default'        => '',
	) );

	$wp_customize->add_control( new tdmagazine_Customize_Textarea_Control( $wp_customize, 'tdmagazine_background_images', array(
		'label'   => __( 'Fixed Image(s)', 'tdmagazine' ),
		'section' => 'tdmagazine_background_section',
		'settings'   => 'tdmagazine_background_images',
	) ) );

	/* Ads Settings */
	$wp_customize->add_section( 'tdmagazine_ads_settings', array(
     	'title'    => __( 'Ads Settings', 'tdmagazine' ),
     	'priority' => 1001,
	) );

	/* Top Ad */
	$wp_customize->add_setting( 'tdmagazine_ads_top_ad', array(
    	'default'        => '',
	) );

	$wp_customize->add_control( new tdmagazine_Customize_Textarea_Control( $wp_customize, 'tdmagazine_ads_top_ad', array(
		'label'   => __( 'Top Ad Section', 'tdmagazine' ),
		'section' => 'tdmagazine_ads_settings',
		'settings'   => 'tdmagazine_ads_top_ad',
	) ) );

	/* Bottom Ad */
	$wp_customize->add_setting( 'tdmagazine_ads_bottom_ad', array(
    	'default'        => '',
	) );

	$wp_customize->add_control( new tdmagazine_Customize_Textarea_Control( $wp_customize, 'tdmagazine_ads_bottom_ad', array(
		'label'   => __( 'Bottom Ad Section', 'tdmagazine' ),
		'section' => 'tdmagazine_ads_settings',
		'settings'   => 'tdmagazine_ads_bottom_ad',
	) ) );

	/* Post Entry Ad */
	$wp_customize->add_setting( 'tdmagazine_ads_post_entry_ad', array(
    	'default'        => '',
	) );

	$wp_customize->add_control( new tdmagazine_Customize_Textarea_Control( $wp_customize, 'tdmagazine_ads_post_entry_ad', array(
		'label'   => __( 'Post Entry Ad Section', 'tdmagazine' ),
		'section' => 'tdmagazine_ads_settings',
		'settings'   => 'tdmagazine_ads_post_entry_ad',
	) ) );

	/* After Post Ad */
	$wp_customize->add_setting( 'tdmagazine_ads_after_post_ad', array(
    	'default'        => '',
	) );

	$wp_customize->add_control( new tdmagazine_Customize_Textarea_Control( $wp_customize, 'tdmagazine_ads_after_post_ad', array(
		'label'   => __( 'After Post Ad Section', 'tdmagazine' ),
		'section' => 'tdmagazine_ads_settings',
		'settings'   => 'tdmagazine_ads_after_post_ad',
	) ) );

	/* Themeforest Section */
	$wp_customize->add_section( 'tdmagazine_themeforest_section', array(
    	'title'          => __( 'Make Some Money', 'tdmagazine' ),
    	'priority'       => 9999,
	) );

	/* User ID */
 	$wp_customize->add_setting( 'tdmagazine_themeforest_user', array(
    	'default'        => '',
	) );

	$wp_customize->add_control( new tdCustom_Text_Control( $wp_customize, 'tdmagazine_themeforest_user', array(
        'label' => __( 'Themeforest Username', 'tdmagazine' ),
        'section' => 'tdmagazine_themeforest_section',
        'settings' => 'tdmagazine_themeforest_user',
        'extra' => __( "Your Themeforest affiliate link will be added to the Footer Credit Text.  If a new user clicks your referrer link and proceeds to sign up an account and purchase an item or deposit money via any Envato Marketplace, you will receive 30% of that person's first cash deposit or purchase price.", "tdmagazine" )
        ) )
    );
}
add_action( 'customize_register', 'tdmagazine_customize_register');

function tdmagazine_header_favicon_sanitize( $input ) {
	$input_filetype = wp_check_filetype( $input );

	if( $input_filetype['ext'] != 'ico' ) {
		$input = '';
		return $input;
	} else {
		return $input;
	}
}

function tdmagazine_sanitize_number( $value ) {
    if( !is_numeric($value) ) {
    	$value = 0;
    }

    return $value;
}

/**
 * Add Textarea to Customizer
 *
 * @since tdmagazine 1.0
 */
if( class_exists( 'WP_Customize_Control' ) ):
	/**
	 * Add Number Picker to the Customizer
	 *
	 * @since tdmagazine 1.0
	 */
	class tdmagazine_Customize_Number_Control extends WP_Customize_Control {
		public $type = 'number';

		public function render_content() {
			?>
			<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<input type="number" <?php $this->link(); ?> value="<?php echo intval( $this->value() ); ?>" />
			</label>
			<?php
		}
	}

	class tdmagazine_Customize_Textarea_Control extends WP_Customize_Control {
		public $type = 'textarea';

		public function render_content() {
			?>
			<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<textarea rows="6" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
			</label>
			<?php
		}
	}

	class tdCustom_Text_Control extends WP_Customize_Control {
		public $type = 'customtext';
		public $extra = '';
		public function render_content() {
		?>
		<label>
			<span><?php echo esc_html( $this->extra ); ?></span>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<input type="text" value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?> />
		</label>
		<?php
		}
	}
endif;
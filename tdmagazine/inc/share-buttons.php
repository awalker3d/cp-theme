<?php

/**
 * Share buttons functionality
 *
 * @package tdmagazine
 */
function tdmagazine_share_buttons() {
	global $post;

	$current_url = get_permalink();
	$current_title = get_the_title();

	$is_custom_style = get_theme_mod( 'tdmagazine_website_settings_share_buttons_style', 'default' );
	$share_butons_title = get_theme_mod( 'tdmagazine_share_button_title', 'Share:' );

	if( get_the_post_thumbnail() ) {
		$currentImage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID, 'large' ) );
	} else {
		$currentImage = array('');
	}

	$twitter_url = 'https://twitter.com/intent/tweet?url='.urlencode( $current_url ).'&amp;text='.urlencode(html_entity_decode($current_title, ENT_COMPAT, 'UTF-8'));
	$facebook_url = 'http://www.facebook.com/share.php?u='.urlencode( $current_url ).'&amp;title='.esc_attr( $current_title );
	$google_plus_url = 'https://plus.google.com/share?url='.urlencode( $current_url );
	$pinterest_url = 'http://pinterest.com/pin/create/button/?url='.urlencode( $current_url ).'&amp;media='.$currentImage[0].'&amp;description='.esc_attr( $current_title );
	$linkedin_url = 'http://www.linkedin.com/shareArticle?mini=true&amp;url='.urlencode( $current_url ).'&amp;title='.esc_attr( $current_title );

	if( $is_custom_style === 'custom' ){
		$output = '<div class="share-section custom-style">';

		$output .= '<span class="share-buttons-title">'.$share_butons_title.'</span><!-- .share-buttons-title -->';
		$output .= '<a href="'.esc_url( $facebook_url ).'" class="facebook"><span class="share-meta">Facebook</span></a>';
		$output .= '<a href="'.esc_url( $google_plus_url ).'" class="googleplus"><span class="share-meta">Google Plus</span></a>';
		$output .= '<a href="'.esc_url( $twitter_url ).'" class="twitter"><span class="share-meta">Twitter</span></a>';
		$output .= '<a href="'.esc_url( $pinterest_url ).'" class="pinterest"><span class="share-meta">Pinterest</span></a>';
		$output .= '<a href="'.esc_url( $linkedin_url ).'" class="linkedin"><span class="share-meta">LinkedIn</span></a>';
	} else {
		$output = '<div class="share-section default-style">';

		$google_plus_share = '<div class="g-plus" data-action="share" data-annotation="bubble"></div>';
		$google_plus_share .= '<script type="text/javascript">';
		$google_plus_share .= '(function() {';
		$google_plus_share .= 'var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;';
		$google_plus_share .= 'po.src = "https://apis.google.com/js/plusone.js";';
		$google_plus_share .= 'var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);';
		$google_plus_share .= '})();';
		$google_plus_share .= '</script>';

		$in_share = '<script src="//platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>';
		$in_share .= '<script type="IN/Share" data-counter="right"></script>';

		$facebook_share = '<iframe src="//www.facebook.com/plugins/like.php?href='.esc_url( $current_url ).'&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:21px;" allowTransparency="true"></iframe>';

		$twitter_share = '<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>';
		$twitter_share .= '<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?"http":"https";if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document, "script", "twitter-wjs");</script>';

		$pinterest = '<a href="http://pinterest.com/pin/create/button/?url='.esc_url( $current_url ).'&media='.$currentImage[0].'&description='.esc_attr( $current_title ).'" class="pin-it-button" count-layout="horizontal">Pin It</a>';
		$pinterest .= '<script type="text/javascript" src="http://assets.pinterest.com/js/pinit.js"></script>';

		$output .= '<ul class="default-share-buttons"><li><div class="share-buttons-title">'.$share_butons_title.'</div></li><li class="facebook">'.$facebook_share.'</li><li class="twitter">'.$twitter_share.'</li><li class="google-plus">'.$google_plus_share.'</li><li class="in">'.$in_share.'</li><li class="pinterest">'.$pinterest.'</li></ul>';
	}

	$output .= '</div><!-- .share-section -->';

	echo $output;
}

/**
 * Share Buttons Top
 *
 * @since tdmagazine 1.2.1
 * @updated tdmagazine 2.0
 */
function tdmagazine_is_share_buttons_top() {
	if( get_theme_mod( 'tdmagazine_website_settings_share_buttons_top', 'off' ) === 'on' ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Share Buttons Bottom
 *
 * @since tdmagazine 1.2.1
 * @updated tdmagazine 2.0
 */
function tdmagazine_is_share_buttons_bottom() {
	if( get_theme_mod( 'tdmagazine_website_settings_share_buttons_bottom', 'on' ) === 'on' ) {
		return true;
	} else {
		return false;
	}
}
<?php

/* Fire our meta box setup function on the post editor screen. */
add_action( 'load-post.php', 'td_post_meta_boxes_setup' );
add_action( 'load-post-new.php', 'td_post_meta_boxes_setup' );

/**
 * Create one or more meta boxes to be displayed on the post editor screen.
 */
function td_add_post_meta_boxes() {
	add_meta_box(
        'td-feature-media',
        esc_html__( 'Featured Media', 'tddailynews' ),
        'td_featured_media',
        'post',
        'side',
        'default'
    );
}

/**
 * Meta box setup function.
 */
function td_post_meta_boxes_setup() {

	/* Add meta boxes on the 'add_meta_boxes' hook. */
	add_action( 'add_meta_boxes', 'td_add_post_meta_boxes' );

	/* Save post meta on the 'save_post' hook. */
	add_action( 'save_post', 'td_save_post_meta', 10, 2 );
}

/**
 *
 * Add custom text field to the Featured Image meta box.
 * Allow user to add image credits to the featured image.
 *
 */
function td_add_featured_image_credits( $content ) {
	global $post;

	ob_start();
	wp_nonce_field( plugin_basename( __FILE__ ), 'td_featured_image_credits_noncename' );
	$nonce = ob_get_clean();

	$field_id = '_td_featured_image_credits';
	$featured_image_credits = get_post_meta( $post->ID, $field_id, true );

	$content .= $nonce;
	$content .= '<p>';
	$content .= '<label for="' . $field_id . '">'.__( 'Image Credits:', 'tddailynews' ).'</label><br>';
	$content .= '<input type="text" id="'.esc_attr( $field_id ).'" name="'.esc_attr( $field_id ).'" value="'.esc_attr( $featured_image_credits ).'">';
	$content .= '</p>';

    return $content;
}
add_filter( 'admin_post_thumbnail_html', 'td_add_featured_image_credits');

/**
 *
 * Allow user to add featured media to the post
 *
 */
function td_featured_media() {
	global $post;

	wp_nonce_field( plugin_basename( __FILE__ ), 'td_featured_media_noncename' );
	$featured_media = get_post_meta( $post->ID, '_td_featured_media', true );
	?>

	<textarea name="_td_featured_media" class="widefat" rows="8"><?php echo $featured_media; ?></textarea>
	<p class="howto"><?php _e( 'Insert your code into the textarea and it will be shown on the video post format page. If you have an optional Homepage template then make sure you have set the featured image as well.', 'tdmagazine' ); ?></p>

	<?php
}

/**
 * Save Custom Metabox Data
 */
 function td_save_post_meta( $post_id, $post ) {

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return  $post->ID;
	}

	if ( isset( $_POST['post_type'] ) && $_POST['post_type'] == 'post' ) {
    	if ( !isset( $_POST['td_featured_image_credits_noncename'] ) || !wp_verify_nonce( $_POST['td_featured_image_credits_noncename'], plugin_basename(__FILE__) )) {
			return $post->ID;
		}

		if ( !isset( $_POST['td_featured_media_noncename'] ) || !wp_verify_nonce( $_POST['td_featured_media_noncename'], plugin_basename(__FILE__) )) {
			return $post->ID;
		}
    }

    $td_custom_data['_td_featured_image_credits'] = isset( $_POST['_td_featured_image_credits'] ) ? $_POST['_td_featured_image_credits'] : '';
    $td_custom_data['_td_featured_media'] = isset( $_POST['_td_featured_media'] ) ? $_POST['_td_featured_media'] : '';

    foreach ( $td_custom_data as $key => $value ) {

        if( $post->post_type == 'revision' ) {
        	return;
        }

        $value = implode(',', (array)$value);

        if( get_post_meta( $post->ID, $key, false ) ) {
            update_post_meta( $post->ID, $key, $value );
        } else {
            add_post_meta( $post->ID, $key, $value );
        }

        if( !$value ) {
        	delete_post_meta( $post->ID, $key );
        }
    }
}
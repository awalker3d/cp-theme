<?php
/**
* Template for Page or Post
*
* @since tdmagazine 1.0
*/
function tdmagazine_post_page_template() {
	$tdmagazine_page_post_template = get_post_meta( get_the_ID(), '_tdmagazine_page_post_template', true );

	$is_sidebar = true;
	$sidebar_right = true;
	$sidebar_left = false;
	$span_class = 'span8';

	if( $tdmagazine_page_post_template === 'left-sidebar' ) {
		$sidebar_right = false;
		$sidebar_left = true;
		$span_class = 'span8 pull-right';
	} else if( $tdmagazine_page_post_template === 'full-width' ) {
		$span_class = 'span12';
		$is_sidebar = false;
	}

	$post_page_template_info = array(
		'is_sidebar' => $is_sidebar,
		'right_sidebar' => $sidebar_right,
		'left_sidebar' => $sidebar_left,
		'span_class' => $span_class
	);

	return $post_page_template_info;
}

/**
 * Social Icons
 *
 * @since tdmagazine 1.0
 */
function tdmagazine_social_links() {
	$twitter = get_theme_mod('tdmagazine_social_twitter');
 	$facebook = get_theme_mod('tdmagazine_social_facebook');
 	$googleplus = get_theme_mod('tdmagazine_social_googleplus');
 	$skype = get_theme_mod('tdmagazine_social_skype');
 	$flickr = get_theme_mod('tdmagazine_social_flickr');
 	$linkedin = get_theme_mod('tdmagazine_social_linkedin');
 	$pinterest = get_theme_mod('tdmagazine_social_pinterest');
 	$rss = get_theme_mod('tdmagazine_social_rss');
 	$picasa = get_theme_mod('tdmagazine_social_picasa');
 	$vimeo = get_theme_mod('tdmagazine_social_vimeo');
 	$youtube = get_theme_mod('tdmagazine_social_youtube');
 	$vimeo = get_theme_mod('tdmagazine_social_vimeo');
 	$instagram = get_theme_mod('tdmagazine_social_instagram');
 	$dribbble = get_theme_mod('tdmagazine_social_dribbble');

 	$output = '';

 	if( !empty( $twitter ) ) {
 		$output .= '<li><a class="tooltip" title="'.esc_attr__( 'Twitter', 'tdmagazine' ).'" href="'.esc_url( $twitter ).'"><i class="fa fa-twitter-square"></i></a></li>';
 	}

 	if( !empty( $facebook ) ) {
 		$output .= '<li><a class="tooltip" title="'.esc_attr__( 'Facebook', 'tdmagazine' ).'" href="'.esc_url( $facebook ).'"><i class="fa fa-facebook-square"></i></a></li>';
 	}

 	if( !empty( $googleplus ) ) {
 		$output .= '<li><a class="tooltip" title="'.esc_attr__( 'Google+', 'tdmagazine' ).'" href="'.esc_url( $googleplus ).'"><i class="fa fa-google-plus-square"></i></a></li>';
 	}

 	if( !empty( $skype ) ) {
 		$output .= '<li><a class="tooltip" title="'.esc_attr__( 'Skype', 'tdmagazine' ).'" href="skype:'.esc_attr( $skype ).'?call"><i class="fa fa-skype"></i></a></li>';
 	}

 	if( !empty( $flickr ) ) {
 		$output .= '<li><a class="tooltip" title="'.esc_attr__( 'Flickr', 'tdmagazine' ).'" href="'.esc_url( $flickr ).'"><i class="fa fa-flickr"></i></a></li>';
 	}

 	if( !empty( $instagram ) ) {
 		$output .= '<li><a class="tooltip" title="'.esc_attr__( 'Instagram', 'tdmagazine' ).'" href="'.esc_url( $instagram ).'"><i class="fa fa-instagram"></i></a></li>';
 	}

 	if( !empty( $linkedin ) ) {
 		$output .= '<li><a class="tooltip" title="'.esc_attr__( 'LinkedIn', 'tdmagazine' ).'" href="'.esc_url( $linkedin ).'"><i class="fa fa-linkedin-square"></i></a></li>';
 	}

 	if( !empty( $pinterest ) ) {
 		$output .= '<li><a class="tooltip" title="'.esc_attr__( 'Pinterest', 'tdmagazine' ).'" href="'.esc_url( $pinterest ).'"><i class="fa fa-pinterest"></i></a></li>';
 	}

 	if( !empty( $dribbble ) ) {
 		$output .= '<li><a class="tooltip" title="'.esc_attr__( 'Dribbble', 'tdmagazine' ).'" href="'.esc_url( $dribbble ).'"><i class="fa fa-dribbble"></i></a></li>';
 	}

 	if( !empty( $rss ) ) {
 		$output .= '<li><a class="tooltip" title="'.esc_attr__( 'RSS', 'tdmagazine' ).'" href="'.esc_url( $rss ).'"><i class="fa fa-rss-square"></i></a></li>';
 	}

 	if( !empty( $youtube ) ) {
 		$output .= '<li><a class="tooltip" title="'.esc_attr__( 'YouTube', 'tdmagazine' ).'" href="'.esc_url( $youtube ).'"><i class="fa fa-youtube-square"></i></a></li>';
 	}

 	if( !empty( $vimeo ) ) {
 		$output .= '<li><a class="tooltip" title="'.esc_attr__( 'Vimeo', 'tdmagazine' ).'" href="'.esc_url( $vimeo ).'"><i class="fa fa-vimeo-square"></i></a></li>';
 	}

 	return $output;
}

/**
 * Latest Posts in Category
 *
 * @since tdmagazine 1.0
 */
function tdmagazine_latest_from_category() {
	global $post;
	//setlocale( LC_TIME, get_locale() );

	$is_custom_categories = get_theme_mod( 'tdmagazine_home_page_categories_display', 'default' );
	$posts_per_category = intval( get_theme_mod( 'tdmagazine_home_page_number_per_category', '7' ) );

	if ( $is_custom_categories === 'default' ) {

		$args = array(
			'orderby' => 'name',
			'parent' => 0
  		);

  		$categories = get_categories( $args );
		$output = '';

		if ( $categories ) {
			foreach ( $categories as $category ) {
				$output .= '<div class="home-category-container">';
				$output .= '<h4 class="home-category-container-title"><a href="'.esc_url(  get_category_link( $category->term_id ) ).'">'.$category->name.'</a></h4>';

				$category_args = array( 'posts_per_page' => $posts_per_category, 'category' => $category->term_id );
				$category_posts = get_posts( $category_args );

				$post_position = 1;
				$post_count = 0;

				foreach( $category_posts as $post ) : setup_postdata( $post );
					if ( $post_position < 2 ) {
						$output .= '<div class="home-category-latest">';
						$output .= '<div class="home-category-image"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_post_thumbnail( $post->ID, 'td-medium-thumb' ).'</a></div>';
						$output .= '<div class="home-category-info">';
						$output .= '<h3 class="home-category-title"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>';
						//$output .= '<p class="home-category-date">'.esc_html( strftime('%B %e, %Y', get_post_time('U', true)) ). '</p>';

						$output .= '<div class="home-category-meta">';
						$output .= '<a href="'.get_permalink().'">';
						$output .= '<span class="home-category-date">'.get_the_date().'</span>';
						$output .= '<span class="home-category-comments"><span class="sep">&mdash;</span><i class="fa fa-comments-o"></i> '.tdmagazine_get_comments().'</span>';
						$output .= '</a>';
						$output .= '</div><!-- .home-category-meta -->';

						$output .= '<div class="home-category-excerpt"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_excerpt().'</a></div>';
						$output .= '</div></div>';
					} else {
						++$post_count;

						if ( $post_count % 2 == 0 ) {
							$output .= '<div class="home-category-others span6">';
							$output .= '<div class="home-category-image"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_post_thumbnail( $post->ID, 'thumbnail' ).'</a></div>';
							$output .= '<div class="home-category-info">';
							$output .= '<h3 class="home-category-title"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>';
							//$output .= '<p class="home-category-date">'.esc_html( strftime('%B %e, %Y', get_post_time('U', true)) ). '</p>';

							$output .= '<div class="home-category-meta">';
							$output .= '<a href="'.get_permalink().'">';
							$output .= '<span class="home-category-date">'.get_the_date().'</span>';
							$output .= '<span class="home-category-comments"><span class="sep">&mdash;</span><i class="fa fa-comments-o"></i> '.tdmagazine_get_comments().'</span>';
							$output .= '</a>';
							$output .= '</div><!-- .home-category-meta -->';

							$output .= '</div></div>';

							$output .= '</div>';
						} else {
							$output .= '<div class="row-fluid">';

							$output .= '<div class="home-category-others span6">';
							$output .= '<div class="home-category-image"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_post_thumbnail( $post->ID, 'thumbnail' ).'</a></div>';
							$output .= '<div class="home-category-info">';
							$output .= '<h3 class="home-category-title"><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
							//$output .= '<p class="home-category-date">'.esc_html( strftime('%B %e, %Y', get_post_time('U', true)) ). '</p>';

							$output .= '<div class="home-category-meta">';
							$output .= '<a href="'.get_permalink().'">';
							$output .= '<span class="home-category-date">'.get_the_date().'</span>';
							$output .= '<span class="home-category-comments"><span class="sep">&mdash;</span><i class="fa fa-comments-o"></i> '.tdmagazine_get_comments().'</span>';
							$output .= '</a>';
							$output .= '</div><!-- .home-category-meta -->';

							$output .= '</div></div>';
						}
					}

					$post_position++;

				endforeach;

				wp_reset_postdata();
				$output .= '</div>';

				if( $post_count % 2 != 0 ) {
					$output .= '</div>';
				}
			}
		}

		echo $output;

	} else {
	 	$custom_categories = get_theme_mod( 'tdmagazine_home_page_categories_display_ids', '' );
	 	$available_cutegories = explode( ",", $custom_categories );

	 	$available_cutegories = tdmagazine_custom_categories_validation( $available_cutegories );

	 	if( $available_cutegories ) {
	 		$output = '';

	 		foreach ( $available_cutegories as $single_category ) {
	 			if( get_category($single_category) ) {
	 				$current_category = get_category( $single_category );

					$output .= '<div class="home-category-container">';
					$output .= '<h4 class="home-category-container-title"><a href="'.esc_url(  get_category_link( $current_category->term_id ) ).'">'.$current_category->name.'</a></h4>';

					$category_args = array( 'posts_per_page' => $posts_per_category, 'category' => $current_category->term_id );
					$category_posts = get_posts( $category_args );

					$post_position = 1;
					$post_count = 0;

					foreach( $category_posts as $post ) : setup_postdata( $post );
						if ( $post_position < 2 ) {
							$output .= '<div class="home-category-latest">';
							$output .= '<div class="home-category-image"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_post_thumbnail().'</a></div>';
							$output .= '<div class="home-category-info">';
							$output .= '<h3 class="home-category-title"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>';
							//$output .= '<p class="home-category-date">'.esc_html( strftime('%B %e, %Y', get_post_time('U', true)) ). '</p>';

							$output .= '<div class="home-category-meta">';
							$output .= '<a href="'.get_permalink().'">';
							$output .= '<span class="home-category-date">'.get_the_date().'</span>';
							$output .= '<span class="home-category-comments"><span class="sep">&mdash;</span><i class="fa fa-comments-o"></i> '.tdmagazine_get_comments().'</span>';
							$output .= '</a>';
							$output .= '</div><!-- .home-category-meta -->';

							$output .= '<div class="home-category-excerpt"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_excerpt().'</a></div>';
							$output .= '</div></div>';
						} else {
							++$post_count;

							if ( $post_count % 2 == 0 ) {
								$output .= '<div class="home-category-others span6">';
								$output .= '<div class="home-category-image"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_post_thumbnail( $post_id, 'thumbnail' ).'</a></div>';
								$output .= '<div class="home-category-info">';
								$output .= '<h3 class="home-category-title"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>';
								//$output .= '<p class="home-category-date">'.esc_html( strftime('%B %e, %Y', get_post_time('U', true)) ). '</p>';

								$output .= '<div class="home-category-meta">';
								$output .= '<a href="'.get_permalink().'">';
								$output .= '<span class="home-category-date">'.get_the_date().'</span>';
								$output .= '<span class="home-category-comments"><span class="sep">&mdash;</span><i class="fa fa-comments-o"></i> '.tdmagazine_get_comments().'</span>';
								$output .= '</a>';
								$output .= '</div><!-- .home-category-meta -->';

								$output .= '</div></div>';

								$output .= '</div>';
							} else {
								$output .= '<div class="row-fluid">';

								$output .= '<div class="home-category-others span6">';
								$output .= '<div class="home-category-image"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_post_thumbnail( $post_id, 'thumbnail' ).'</a></div>';
								$output .= '<div class="home-category-info">';
								$output .= '<h3 class="home-category-title"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>';
								//$output .= '<p class="home-category-date">'.esc_html( strftime('%B %e, %Y', get_post_time('U', true)) ). '</p>';

								$output .= '<div class="home-category-meta">';
								$output .= '<a href="'.get_permalink().'">';
								$output .= '<span class="home-category-date">'.get_the_date().'</span>';
								$output .= '<span class="home-category-comments"><span class="sep">&mdash;</span><i class="fa fa-comments-o"></i> '.tdmagazine_get_comments().'</span>';
								$output .= '</a>';
								$output .= '</div><!-- .home-category-meta -->';

								$output .= '</div></div>';
							}
						}

						$post_position++;

					endforeach;

					wp_reset_postdata();
					$output .= '</div>';

					if( $post_count % 2 != 0 ) {
						$output .= '</div>';
					}
	 			}
	 		}
	 		echo $output;
	 	}
	}
}

/**
 * Custom Category validation
 *
 * @since tdmagazine 1.0
 */
function tdmagazine_custom_categories_validation( $categories ) {
	$valid_calegories = array();

	foreach ( $categories as $category ) {
		if( get_term_by( 'id', $category, 'category') && !empty($category) ) {
			$valid_calegories[] = trim( $category );
		}
	}

	return array_unique( $valid_calegories );
}

if ( ! function_exists( 'tdmagazine_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
function tdmagazine_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;

	if ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<div class="comment-body">
			<?php _e( 'Pingback:', 'tdmagazine' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( 'Edit', 'tdmagazine' ), '<span class="edit-link">', '</span>' ); ?>
		</div>

	<?php else : ?>

	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
				<div class="comment-author vcard">
					<div class="avatar-container"><?php echo get_avatar( $comment, 92 ); ?></div>
					<div><?php printf( __( '%s ', 'tdmagazine' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?></div>
					<p class="comment-meta commentmetadata">
						<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>"><time datetime="<?php comment_time( 'c' ); ?>">
							<?php
								setlocale( LC_TIME, get_locale() );
								/* translators: 1: date, 2: time */
								printf( __( '%1$s at %2$s', 'tdmagazine' ), strftime( '%B %e, %Y', get_comment_date('U') ), get_comment_time() ); ?>
							</time>
						</a>
						<?php edit_comment_link( __( '(Edit)', 'tdmagazine' ), ' ' ); ?>
					</p>
				</div><!-- .comment-author .vcard -->
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<br />
					<span class="secondary label"><?php _e( 'Your comment is awaiting moderation.', 'tdmagazine' ); ?></span>
				<?php endif; ?>

			<div class="comment-content"><?php comment_text(); ?></div>

			<footer>
				<div class="comment-reply-link-area">
				<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text'=> __( 'Reply', 'tdmagazine' ) ) ) ); ?><!-- .comment-reply-link -->
				</div>
			</footer>

		</article><!-- #comment-## -->

	<?php
	endif;
}
endif; // ends check for tdmagazine_comment()

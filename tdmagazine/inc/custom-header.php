<?php
/**
 * Setup the WordPress core custom header feature.
 *
 * @uses tdmagazine_header_style()
 * @uses tdmagazine_admin_header_style()
 * @uses tdmagazine_admin_header_image()
 */
function tdmagazine_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'tdmagazine_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => '333333',
		'width'                  => 0,
		'height'                 => 0,
		'flex-height'            => false,
		'uploads'                => false,
		'wp-head-callback'       => 'tdmagazine_header_style',
		'admin-head-callback'    => 'tdmagazine_admin_header_style',
		'admin-preview-callback' => 'tdmagazine_admin_header_image',
	) ) );
}
add_action( 'after_setup_theme', 'tdmagazine_custom_header_setup' );

if ( ! function_exists( 'tdmagazine_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog
 *
 * @see tdmagazine_custom_header_setup().
 */
function tdmagazine_header_style() {
	$header_text_color = get_header_textcolor();

	// If no custom options for text are set, let's bail
	// get_header_textcolor() options: HEADER_TEXTCOLOR is default, hide text (returns 'blank') or any hex value
	if ( HEADER_TEXTCOLOR == $header_text_color ) {
		return;
	}

	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css">
	<?php
		// Has the text been hidden?
		if ( 'blank' == $header_text_color ) :
	?>
		.site-title,
		.site-description {
			position: absolute;
			clip: rect(1px, 1px, 1px, 1px);
		}
	<?php
		// If the user has set a custom color for the text use that
		else :
	?>
		#masthead a,
		#masthead .site-title a,
		#masthead .site-description,
		#header-searchform,
		#secondary-navigation a {
			color: #<?php echo esc_attr( $header_text_color ); ?>;
		}
	<?php endif; ?>
	</style>
	<?php
}
endif; // tdmagazine_header_style

if ( ! function_exists( 'tdmagazine_admin_header_style' ) ) :
/**
 * Styles the header image displayed on the Appearance > Header admin panel.
 *
 * @see tdmagazine_custom_header_setup().
 */
function tdmagazine_admin_header_style() {
?>
	<style type="text/css">
		.appearance_page_custom-header #headimg {
			border: none;
		}

		#headimg h1 {
			font-size: 32px;
			line-height: 1.3;
			margin-top: 0;
			margin-bottom: 0;
			font-weight: 900;
		}

		#headimg h1 a {
			text-decoration: none;
		}

		#headimg h1 a,
		#desc {
			color: #ffffff;
		}

		#desc {
			font-size: 11px;
			margin: 15px 0 5px;
			opacity: 0.6;
			text-transform: uppercase;
		}
	</style>
<?php
}
endif; // tdmagazine_admin_header_style

if ( ! function_exists( 'tdmagazine_admin_header_image' ) ) :
/**
 * Custom header image markup displayed on the Appearance > Header admin panel.
 *
 * @see tdmagazine_custom_header_setup().
 */
function tdmagazine_admin_header_image() {
?>
	<div id="headimg">
		<?php if ( get_header_image() ) : ?>
			<div><img src="<?php echo esc_url( get_header_image() ); ?>" alt=""></div>
		<?php endif; ?>
		<h1 class="displaying-header-text"><a id="name"<?php echo sprintf( ' style="color:#%s;"', get_header_textcolor() ); ?> onclick="return false;" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
		<div class="displaying-header-text" id="desc"<?php echo sprintf( ' style="color:#%s;"', get_header_textcolor() ); ?>><?php bloginfo( 'description' ); ?></div>
	</div><!-- #headimg -->
<?php
}
endif; // tdmagazine_admin_header_image

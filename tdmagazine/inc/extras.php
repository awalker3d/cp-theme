<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package tdmagazine
 */

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 */
function tdmagazine_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'tdmagazine_page_menu_args' );

/**
 * Adds custom classes to the array of body classes.
 */
function tdmagazine_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	if(	tdmagazine_is_narrow_site_width() ) {
		$classes[] = 'narrow-width';
	}

	return $classes;
}
add_filter( 'body_class', 'tdmagazine_body_classes' );

/**
 * Adds custom classes to the array of post classes.
 */
function tdmagazine_post_classes( $classes ) {
	if( tdmagazine_is_inline_layout() ) {
		$classes[] = 'inline-hentry';
	}

	return $classes;
}
add_filter( 'post_class', 'tdmagazine_post_classes' );


/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function tdmagazine_wp_title( $title, $sep ) {
	if ( is_feed() ) {
		return $title;
	}

	global $page, $paged;

	// Add the blog name
	$title .= get_bloginfo( 'name', 'display' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title .= " $sep $site_description";
	}

	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title .= " $sep " . sprintf( __( 'Page %s', 'tdmagazine' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'tdmagazine_wp_title', 10, 2 );

 /**
 * Checks if user wants to have social links in the header section
 *
 * @since tdmagazine 1.1
 * @updated tdmagazine 2.0
 */
function tdmagazine_is_social_top() {
	if( get_theme_mod( 'tdmagazine_social_top', 'off' ) === 'on' ) {
		return true;
	} else {
		return false;
	}
}

 /**
 * Checks if user wants to have social links in the footer section
 *
 * @since tdmagazine 1.1
 * @updated tdmagazine 2.0
 */
function tdmagazine_is_social_bottom() {
	if( get_theme_mod( 'tdmagazine_social_bottom', 'on' ) === 'on' ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Return a column class for the primary blog container
 *
 * @since tdmagazine 2.0
 */
function tdmagazine_get_blog_primary_class() {
	if( !tdmagazine_is_sidebar_hidden() ) {
		if( tdmagazine_is_left_sidebar() ) {
			return 'col-lg-8 col-md-8 pull-right';
		} else {
			return 'col-lg-8 col-md-8';
		}
	} else {
		return 'col-lg-12';
	}
}

/**
 * Check if user wants to hide a sidebar on a site.
 *
 * @since tdmagazine 2.0
 */
function tdmagazine_is_sidebar_hidden() {
	$page_template = get_post_meta( get_the_ID(), '_tdmagazine_page_post_template', true );
	$is_blog_sidebar = get_theme_mod( 'tdmagazine_blog_sidebar', '1' );
	$is_singular_sidebar = get_theme_mod( 'tdmagazine_single_post_sidebar', '1' );

	//is Homepage
	if( is_front_page() ) {

		if( is_active_sidebar( 'sidebar-1' ) && $page_template != 'full-width' ) {
			return false;
		} else {
			return true;
		}

	// is single page
	} elseif( is_singular() ) {

		if( is_single() && !$is_singular_sidebar ) {
			return true;
		}

		if( $page_template === 'full-width' || !is_active_sidebar( 'sidebar-1' ) ) {
			return true;
		} else {
			return false;
		}

	// is posts page
	} elseif( is_home() || is_search() || is_archive() ) {

		if( is_active_sidebar( 'sidebar-1' ) && $is_blog_sidebar ) {
			return false;
		} else {
			return true;
		}

	} else {

		if( is_active_sidebar( 'sidebar-1' ) ) {
			return false;
		} else {
			return true;
		}

	}
}

/**
 * Check if user wants to move the sidebar to the left
 *
 * @since tdmagazine 2.0
 */
function tdmagazine_is_left_sidebar() {
	$page_template = get_post_meta( get_the_ID(), '_tdmagazine_page_post_template', true );
	if( $page_template === 'left-sidebar' ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Check if user wants to have two columns layout for the posts page
 *
 * @since tdmagazine 2.0
 */
function tdmagazine_is_two_columns_layout() {
	$selected_layout = get_theme_mod( 'tdmagazine_website_settings_blog_style', 'one-col' );

	if( $selected_layout === 'two-col' || $selected_layout === 'dynanic' ){
		return true;
	} else {
		return false;
	}
}

/**
 * Check if user wants to have three columns layout for the posts page
 *
 * @since tdmagazine 2.0
 */
function tdmagazine_is_three_columns_layout() {
	if( get_theme_mod( 'tdmagazine_website_settings_blog_style', 'one-col' ) === 'three-col' ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Check if user wants to have two columns layout for the posts page
 *
 * @since tdmagazine 2.0
 */
function tdmagazine_is_inline_layout() {
	$selected_layout = get_theme_mod( 'tdmagazine_website_settings_blog_style', 'one-col' );

	if( $selected_layout === 'inline' || $selected_layout === 'thumb-left' ){
		return true;
	} else {
		return false;
	}
}

/**
 * Check if user wants to have narrow site width
 *
 * @since tdmagazine 2.0
 */
function tdmagazine_is_narrow_site_width() {
	if( get_theme_mod( 'tdmagazine_website_width', '' ) != '' ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Featured image credits
 *
 * @since tdmagazine 1.0
 */
function tdmagazine_featured_image_credits() {
	return get_post_meta( get_the_ID() , '_td_featured_image_credits', true );
}

/**
 * Author Section
 *
 * @since tdmagazine 1.0
 * @updated tdmagazine 2.0
 */
function tdmagazine_author_section() {
	$is_author_section = get_theme_mod( 'tdmagazine_website_settings_author_section', 'on' );

	if( $is_author_section === 'on' ) {
		global $post;

		$author_twitter = get_the_author_meta( 'twitter', $post->post_author );
		$author_facebook = get_the_author_meta( 'facebook', $post->post_author );
		$author_googleplus = get_the_author_meta( 'gplus', $post->post_author );
		$author_linkedin = get_the_author_meta( 'linkedin', $post->post_author );
		$author_instagram = get_the_author_meta( 'instagram', $post->post_author );

		$author_twitter_link = '';
		$author_facebook_link = '';
		$author_googleplus_link = '';
		$author_linkedin_link = '';
		$author_instagram_link = '';

		if( $author_twitter != '' ) {
			$author_twitter_link = '<a href="'.esc_url( 'https://twitter.com/'.$author_twitter ).'" class="pull-right" target="_blank" rel="bookmark"><i class="fa fa-twitter"></i> Twitter</a>';
		}

		if( $author_facebook != '' ) {
			$author_facebook_link = '<a href="'.esc_url( $author_facebook ).'" class="pull-right" target="_blank" rel="bookmark"><i class="fa fa-facebook-square"></i> Facebook</a>';
		}

		if( $author_googleplus != '' ) {
			$author_googleplus_link = '<a href="'.esc_url( $author_googleplus ).'" class="pull-right" target="_blank" rel="bookmark"><i class="fa fa-google-plus-square"></i> Google Plus</a>';
		}

		if( $author_linkedin != '' ) {
			$author_linkedin_link = '<a href="'.esc_url( $author_linkedin ).'" class="pull-right" target="_blank"><i class="fa fa-linkedin-square"></i> LinkedIn</a>';
		}

		if( $author_instagram != '' ) {
			$author_instagram_link = '<a href="'.esc_url( $author_instagram ).'" class="pull-right" target="_blank"><i class="fa fa-instagram"></i> Instagram</a>';
		}

		$author_social_links = $author_instagram_link . $author_linkedin_link . $author_googleplus_link . $author_twitter_link . $author_facebook_link ;
		?>
		<div class="author-section">
			<div class="gravatar">
				<?php echo get_avatar( $post->post_author, 128 ); ?>
			</div><!-- .gravatar -->
			<div class="about">
				<h4 class="author-name"><a href="<?php echo esc_attr( get_author_posts_url( $post->post_author ) ); ?>" rel="author"><?php echo get_the_author(); ?></a></h4>
				<p><?php echo nl2br( get_the_author_meta( 'description', $post->post_author ) ); ?></p>
			</div><!-- .about -->
			<?php if( $author_social_links != '' ): ?>
				<div class="author-social-links clearfix"><?php echo $author_social_links; ?></div>
			<?php endif; ?>
		</div><!-- .author-section -->
		<?php
	}
}

/**
 *  This function return list of Authors
 *
 * @since tdmagazine 1.0
 * @updated tdmagazine 2.0
 */
function tdmagazine_get_authors_list() {
	$per_page = intval( get_theme_mod( 'tdmagazine_website_settings_authors_perpage', '10' ) );
	$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$offset = ($page - 1) * $per_page;

	$users = new WP_User_Query( array(
		'orderby' => 'display_name',
		'offset' => $offset,
		'number' => $per_page,
		'who' => 'authors'
	));

	$total_users = $users->total_users;
	$authors = $users->get_results();

	$total_pages = ceil( $total_users / $per_page );

	if ( !empty( $authors ) ):
		foreach( $authors as $author ): ?>
		<div class="authors-list-container">
			<div class="row">
				<div class="col-lg-5 col-md-5 authors-list-about">
					<?php echo get_avatar( $author->ID, 126 ); ?>
					<h4 class="author-name"><?php echo $author->display_name; ?></h4>
					<p><?php echo nl2br( get_the_author_meta( 'description', $author->ID ) ); ?></p>
				</div><!-- .authors-list-about -->
				<div class="col-lg-7 col-md-7 authors-list-recent-articles">
					<h5 class="authors-list-ra-title"><?php _e( 'Recent Articles', 'tdmagazine' ); ?></h5>
					<?php echo tdmagazine_authors_posts( $author ); ?>
				</div><!-- .authors-list-recent-articles -->
			</div><!-- .row -->
		</div><!-- .authors-list-container -->
	<?php endforeach;
	endif;

	if ( $total_pages > 1 ) {
		echo '<div class="page-pagination-conatiner">';
		echo paginate_links(array(
			'base' => get_pagenum_link(1) . '%_%',
			'format' => 'page/%#%',
			'current' => $page,
			'total' => $total_pages,
		));
		echo '</div>';
	}

	wp_reset_postdata();
}

/**
 * This function return list of Authors
 *
 * @since tdmagazine 1.0
 * @updated tdmagazine 2.0
 */
function tdmagazine_authors_posts( $author ) {
	global $post;
	$authors_posts = get_posts( array( 'author' => $author->ID, 'posts_per_page' => 15, 'post__not_in' => array( $post->ID ) ) );
	?>
	<ul>
		<?php foreach( $authors_posts as $authors_post ): ?>
			<li>
				<a href="<?php echo esc_url( get_permalink( $authors_post->ID ) ); ?>"><?php echo apply_filters( 'the_title', $authors_post->post_title, $authors_post->ID ); ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
	<div class="author-link">
		<a href="<?php echo esc_url( get_author_posts_url( $author->ID ) ); ?>">
		<?php echo __( 'View all posts by', 'tdmagazine' ) . ' '. $author->display_name; ?>
		</a>
	</div><!-- .author-link -->
	<?php

    return $output;
}

/**
 * Related Posts Section
 *
 * @since tdmagazine 1.0
 * @updated tdmagazine 2.0
 */
function tdmagazine_related_posts() {
	if( get_theme_mod( 'tdmagazine_website_settings_related_posts_section', 'on' ) === 'on' ) {
		global $post;

		$categories = get_the_category( $post->ID );

		if ( $categories ) {
			$category_ids = array();

			foreach( $categories as $individual_category ) {
				$category_ids[] = $individual_category->term_id;
			}

			$args = array(
				'category__in' => $category_ids,
				'post__not_in' => array( $post->ID ),
				'posts_per_page'=> 3
			);

			$related_posts_query = new wp_query( $args );

			if( $related_posts_query->have_posts() ): ?>
				<div class="related-posts-container">
					<h4 class="related-posts-title accent-color"><?php _e( 'Related Articles', 'tdmagazine' ); ?></h4>
					<ul class="list-unstyled">
						<div class="row">
						<?php while( $related_posts_query->have_posts() ): $related_posts_query->the_post(); ?>
							<li class="col-lg-4 col-md-4">
								<?php if( has_post_thumbnail() ): ?>
								<div class="related-post-entry-image">
									<a href="<?php echo esc_url( get_permalink() ); ?>" class="thumb-link" rel="bookmark" title="<?php echo esc_attr( get_the_title() ); ?>">
									<?php echo get_the_post_thumbnail( $related_posts_query->post->ID, 'td-medium-thumb' ) ?>
									</a><!-- .thumb-link -->
								</div><!-- .related-post-entry-image -->
								<?php endif; ?>
								<h3 class="related-post-entry-title">
									<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark" title="<?php echo esc_attr( get_the_title() ); ?>">
									<?php the_title(); ?>
									</a>
								</h3><!-- .related-post-entry-title -->
								<div class="entry-category">
								<span class="related-meta"><?php _e( 'Posted In:', 'tdmagazine' ); ?></span>
								<?php echo get_the_category_list( ', ' ); ?>
								</div><!-- .entry-category -->
							</li><!-- .col -->
						<?php endwhile; ?>
						</div><!-- .row -->
					</ul><!-- .list-unstyled -->
				</div><!-- .related-posts-container -->
			<?php
			endif;
		}

		wp_reset_postdata();
	}
}


/**
 * Featured Posts Section
 *
 * @since tdmagazine 1.0
 * @updated tdmagazine 2.0
 */
function tdmagazine_featured_posts() {

	$featured_posts_per_page = intval( get_theme_mod( 'tdmagazine_home_page_featured_number', '5' ) );
	$featured_slideshow_speed = intval( get_theme_mod( 'tdmagazine_home_page_featured_speed', '7000' ) );

	$args = array(
		'post_status' => 'publish',
		'post_type' => array('post', 'page'),
		'orderby' => 'post_date',
		'order' => 'DESC',
		'post__not_in'	 =>	get_option("sticky_posts"),
		'posts_per_page' => $featured_posts_per_page,
		'meta_query' => array(
         	array(
            	'key' => '_tdmagazine_featured_post',
            	'value' => '1'
			)
		)
	);

	$featured_content = new WP_Query( $args );

	if( is_front_page() && tdmagazine_is_sidebar_hidden() ) {
		$featured_image_width = 'standard-image-wide';
	} else {
		$featured_image_width = 'standard-image';
	}

	if( $featured_content->have_posts() && is_front_page() ):
		$count = 0; ?>
		<ul class="bxslider list-unstyled">
			<?php while( $featured_content->have_posts() ) : $featured_content->the_post(); ?>
			<li>
				<?php if( has_post_thumbnail() ): ?>
					<?php echo get_the_post_thumbnail( $featured_content->post->ID, $featured_image_width ) ?>
				<?php endif; ?>
				<div class="featured-info">
					<h3 class="featured-title">
						<a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
							<?php the_title(); ?>
						</a>
					</h3>
					<p class="featured-excerpt"><?php echo get_the_excerpt(); ?></p>
				</div><!-- .featured-info -->
			</li>
			<?php
				$count++;
				endwhile;
			?>
		</ul><!-- .bxslider -->
		<?php

		wp_reset_postdata();

		//if( $count > 1 ):
		?>
		<script type="text/javascript">
		jQuery(document).ready(function(){
			var sliderContent = jQuery('.featured-container .bxslider');
			sliderContent.imagesLoaded(function() {
				sliderContent.bxSlider({
				  mode: 'fade',
				  pager: true,
				  adaptiveHeight: true,
				  auto: true,
				  autoHover: true,
				  pause: <?php echo $featured_slideshow_speed; ?>,
				  prevText: '<i class="fa fa-chevron-left"></i>',
				  nextText: '<i class="fa fa-chevron-right"></i>',
				  onSliderLoad: function() {
					sliderContent.css("visibility", "visible");
				  }
				});
			});
		});
		</script>
		<?php
		//endif;
	endif;
}

/**
 * Check if current post can have a slideshow
 *
 * @since tdmagazine 2.0
 */
function tdmagazine_has_post_slideshow() {
	global $post;

	if( has_post_format( 'gallery', $post->ID ) && has_shortcode( $post->post_content, 'gallery' ) ){
		return true;
	} else {
		return false;
	}
}

/**
 * Return only full size images for the post slideshow
 *
 * @since tdmagazine 2.0
 */
function tdmagazine_force_full_images( $content, $pairs, $atts ) {
	if( tdmagazine_has_post_slideshow() ) {
		$content['size'] = 'full';
	}
  	return $content;
}
add_filter('shortcode_atts_gallery','tdmagazine_force_full_images',10,3);

 /**
 * Post Slideshow
 *
 * @since tdmagazine 1.0
 * @updated tdmagazine 2.0
 */
function tdmagazine_post_slideshow() {
	global $post;

	$gallery = get_post_gallery_images( $post );
	if( !empty( $gallery ) ): ?>
		<ul class="bxslider list-unstyled">
		<?php foreach( $gallery as $image ): ?>
			<li>
				<img src="<?php echo esc_url( $image ); ?>" alt="<?php echo esc_attr( $post->post_title ); ?>">
			</li>
		<?php endforeach; ?>
		</ul><!-- .list-unstyled -->
	<?php
	endif;
}

/**
 * Check if current post can have video
 *
 * @since tdmagazine 2.0
 */
function tdmagazine_has_post_video() {
	global $post;
	$video_content = get_post_meta( $post->ID, '_td_featured_media', true );

	if( has_post_format( 'video', $post->ID ) && !empty( $video_content ) ){
		return true;
	} else {
		return false;
	}
}

/**
 * Post Video content
 *
 * @since tdmagazine 2.0
 */
function tdmagazine_post_video() {
	global $post;
	echo get_post_meta( $post->ID, '_td_featured_media', true );
}

 /**
 * Custom Favicon
 *
 * @since tdmagazine 1.0
 * @updated tdmagazine 2.0
 */
function tdmagazine_custom_favicon() {
	$tdmagazine_favicon = get_theme_mod('tdmagazine_header_favicon_img', '');

	if ( $tdmagazine_favicon ) {
		echo '<link rel="shortcut icon" href="'.esc_url( $tdmagazine_favicon ).'" title="Favicon" />' . "\n";
	}
}
add_action('wp_head', 'tdmagazine_custom_favicon');

/**
 * Checks if user want to have a Numeric Navigation
 *
 * @since tdmagazine 2.0
 */
function tdmagazine_is_numeric_navigation() {
	return get_theme_mod( 'tdmagazine_numeric_navigation', '0' );
}

/**
 * Numeric Paging Navigation
 *
 * @since tdmagazine 2.0
 */
function tdmagazine_numeric_pagination($pages = '', $range = 2) {
	$showitems = ($range * 2)+1;

     global $paged;
     if( empty( $paged ) ) {
     	$paged = 1;
     }

     if( $pages == '' ) {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if( !$pages ) {
             $pages = 1;
         }
     }

     if ( 1 != $pages ) {
         echo "<div class='numeric-pagination clearfix'>";

         if( $paged > 2 && $paged > $range+1 && $showitems < $pages ) {
         	echo "<a href='".esc_url( get_pagenum_link(1) )."'>&laquo;</a>";
         }

         if( $paged > 1 && $showitems < $pages ) {
         	echo "<a href='".esc_url( get_pagenum_link($paged - 1) )."'>&lsaquo;</a>";
         }

         for ( $i=1; $i <= $pages; $i++ ) {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".esc_url( get_pagenum_link($i) )."' class='inactive' >".$i."</a>";
             }
         }

         if ( $paged < $pages && $showitems < $pages ) {
         	echo "<a href='".esc_url( get_pagenum_link($paged + 1) )."'>&rsaquo;</a>";
         }

         if ( $paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages ) {
         	echo "<a href='".esc_url( get_pagenum_link($pages) )."'>&raquo;</a>";
         }

         echo "</div><!-- .numeric-pagination -->\n";
     }
}
















/**
 * Breaking News Section
 *
 * @since tdmagazine 1.0
 */
function tdmagazine_breaking_news() {
	$is_breaking_news = get_theme_mod( 'tdmagazine_website_settings_breaking_news_section', 'on' );
	$breaking_news_per_page = intval( get_theme_mod( 'tdmagazine_website_breaking_news_number', '5' ) );

	if( $is_breaking_news === 'on' ) {
		global $post;

		$args = array(
					'post_status' => 'publish',
					'post_type' => array('post', 'page'),
					'orderby' => 'post_date',
					'order' => 'DESC',
					"post__not_in"	 =>	get_option("sticky_posts"),
					'posts_per_page' => $breaking_news_per_page,
					'meta_query' => array(
         				array(
            					'key' => '_tdmagazine_breaking_news',
            					'value' => '1'
							)
					 )
				);

		$breaking_news = new WP_Query( $args );

		if( $breaking_news->have_posts() ) {
			$output = '<div class="breaking-news-container"><ul id="breaking-news-tems" data-breaking-news-title="'.__( 'Breaking News', 'tdmagazine' ).'">';
			while( $breaking_news->have_posts() ) : $breaking_news->the_post();
				$output .= '<li><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></li>';
			endwhile;
			$output .= '</ul></div>';
			echo $output;
		}
		wp_reset_postdata();
	}
}

/**
 * Checks if breaking news section is enabled.
 *
 * @since tdmagazine 1.0
 */
function tdmagazine_is_breaking_news() {
	$is_breaking_news = get_theme_mod( 'tdmagazine_website_settings_breaking_news_section', 'on' );

	if( $is_breaking_news === 'on' ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Checks if user wants to have a fixed menu or not
 *
 * @since tdmagazine 1.0
 */
 function tdmagazine_is_fixed_menu() {
 	$tdmagazine_fixed_menu = get_theme_mod( 'tdmagazine_nav_fixed_menu', 'off' );

 	if( $tdmagazine_fixed_menu == 'on' ) {
 		return true;
 	} else {
 		return false;
 	}
 }



/**
*  This function adds custom styles to website head.
*
* @since tdmagazine 1.0
*/
function tdmagazine_custom_styles() {
	$tdmagazine_background_color = get_theme_mod( 'tdmagazine_background_color', '#e9e9e9' );

	if( $tdmagazine_background_color != '#e9e9e9' ) {
		$tdmagazine_background_color = " body {background-color: ". $tdmagazine_background_color .";} \n ";
	} else {
		$tdmagazine_background_color = '';
	}

	$tdmagazine_background_pattern = get_theme_mod( 'tdmagazine_background_pattern', '' );
	$tdmagazine_background_images = get_theme_mod( 'tdmagazine_background_images', '' );

	if( $tdmagazine_background_pattern != '' && $tdmagazine_background_images == '' ) {
		$tdmagazine_background_pattern = " body {background: url(". $tdmagazine_background_pattern .");} \n ";
	} else {
		$tdmagazine_background_pattern = '';
	}

	$tdmagazine_custom_style = $tdmagazine_background_color.
							$tdmagazine_background_pattern;

	if( $tdmagazine_custom_style != '' ) {
		echo "
			<style type='text/css'> \n
				" . $tdmagazine_custom_style . "
			</style>
	     ";
	} else {
		return false;
	}
}

 /**
 * Returns Website Background
 *
 * @since tdmagazine 1.0
 */
 function tdmagazine_is_background_image() {
 	$tdmagazine_background_images = get_theme_mod( 'tdmagazine_background_images', '' );

 	if( $tdmagazine_background_images != '' ) {
		$images = explode( ",", $tdmagazine_background_images );
		return json_encode( $images );
	} else {
		return false;
	}
 }

 /**
 * Returns Newsletter Form
 *
 * @since tdmagazine 1.0
 */
 function tdmagazine_newsletter_form() {
 	$tdmagazine_newsletter_code = get_theme_mod( 'tdmagazine_newsletter_form', '' );
 	$tdmagazine_newsletter_image = get_theme_mod( 'tdmagazine_newsletter_image', '' );
 	$temp_array = array();

 	if( $tdmagazine_newsletter_code != '' ) {
 		$temp_array['newsletter-code'] = $tdmagazine_newsletter_code;

 		if( $tdmagazine_newsletter_image != '' ) {
 			$temp_array['newsletter-image'] = $tdmagazine_newsletter_image;
 		} else {
 			$temp_array['newsletter-image'] = '';
 		}

 		return $temp_array;
 	} else {
 		return false;
 	}
 }

 /**
 * Returns Newsletter Image
 *
 * @since tdmagazine 1.0
 */
 function tdmagazine_newsletter_image() {
 	$tdmagazine_newsletter = tdmagazine_newsletter_form();

	if( $tdmagazine_newsletter['newsletter-image'] != '' ) {
		return '
					<div class="newsletter-image">
						<img src="'.esc_url( $tdmagazine_newsletter['newsletter-image'] ).'" alt="">
					</div>
			   ';
	} else {
		return false;
	}
 }

/**
* Website Color Style
*
* @since tdmagazine 1.0
*/
function tdmagazine_website_style() {
	return get_theme_mod( 'tdmagazine_website_style' );
}

/**
* Ads Section
*
* @since tdmagazine 1.0
*/
function tdmagazine_ad_section( $position ) {
	$top_ad = get_theme_mod( 'tdmagazine_ads_top_ad', '' );
	$bottom_ad = get_theme_mod( 'tdmagazine_ads_bottom_ad', '' );
	$post_ad = get_theme_mod( 'tdmagazine_ads_after_post_ad', '' );

	if( $position === 'top' && $top_ad != '' ) {
		$output = '<div id="top-ad-section">';
		$output .= '<p class="ad-title">'.__( 'Advertisement', 'tdmagazine' ).'</p>';
		$output .= $top_ad;
		$output .= '</div>';
		echo $output;
	} else if( $position === 'post'  && $post_ad != '' ) {
		$output = '<div id="post-ad-section">';
		$output .= '<p class="ad-title">'.__( 'Advertisement', 'tdmagazine' ).'</p>';
		$output .= $post_ad;
		$output .= '</div>';
		echo $output;
	} else if( $position === 'bottom'  && $bottom_ad != '' ) {
		$output = '<div id="bottom-ad-section">';
		$output .= '<p class="ad-title">'.__( 'Advertisement', 'tdmagazine' ).'</p>';
		$output .= $bottom_ad;
		$output .= '</div>';
		echo $output;
	} else {
		echo '';
	}
}

/**
* Blog Style
*
* @since tdmagazine 1.0
*/
function tdmagazine_blog_style() {
	$blog_style = get_theme_mod( 'tdmagazine_website_settings_blog_style', 'dynamic' );

	if( $blog_style === 'classic' || $blog_style === 'thumb-left' ) {
		$output = 'span12';
	} else {
		$output = 'span6';
	}

	return $output;
}

/**
* Adds additional class to grid container if
* user wants to have classic blog style
*
* @since tdmagazine 1.0
*/
function tdmagazine_get_blog_style() {
	$blog_style = get_theme_mod( 'tdmagazine_website_settings_blog_style', 'dynamic' );

	if( $blog_style === 'classic' ) {
		return 'classic-blog-style';
	}  else if ( $blog_style === 'thumb-left' ) {
		return 'thumb-left-blog-style';
	} else {
		return 'dynamic-blog-style';
	}
}

/**
 * Checks if user wants to have auto summary
 *
 * @since tdmagazine 1.2.1
 */
 function tdmagazine_is_auto_summary() {
 	$tdmagazine_auto_summary = get_theme_mod( 'tdmagazine_website_settings_auto_summary', 'off' );

 	if( $tdmagazine_auto_summary == 'on' ) {
 		return true;
 	} else {
 		return false;
 	}
 }

/**
 * Returns Auto Summmary Length
 *
 * @since tdmagazine 1.2.1
 */
 function tdmagazine_auto_summary_length() {
 	return get_theme_mod( 'tdmagazine_website_settings_auto_summary_length', '55' );
 }

 /**
 * Affiliate Link
 *
 * @since tdmagazine 1.2.1
 */
function tdmagazine_affiliate_link() {
	$is_affiliate_link = get_theme_mod( 'tdmagazine_themeforest_user', '' );

	if( $is_affiliate_link != '' ) {
		return '<a href="http://themeforest.net/item/tdmagazine-wordpress-news-theme/5087049?ref='.$is_affiliate_link.'" target="_blank">tdMagazine</a> Theme';
	} else {
		return 'tdMagazine Theme';
	}
}

 /**
 * Share buttons style
 *
 * @since tdmagazine 1.2.2
 */
function tdmagazine_is_default_share_buttons() {
	$share_buttons_style = get_theme_mod( 'tdmagazine_website_settings_share_buttons_style', 'default' );

	if( $share_buttons_style === 'default' ) {
		return true;
	} else {
		return false;
	}
}

/**
 *	Custom Meta Box that allows to save the post color.
 *	@since tdmagazine 1.0
 */
 function tdmagazine_custom_metabox() {
	add_meta_box('tdmagazine-breaking-news-settings', 'Breaking News', 'tdmagazine_breaking_news_page_ui', 'post', 'side', 'high');
	add_meta_box('tdmagazine-featured-post-settings', 'Featured Post', 'tdmagazine_featured_post_page_ui', 'post', 'side', 'high');

	add_meta_box('tdmagazine-page-post-template-settings', 'Post Template', 'tdmagazine_page_post_template_ui', 'post', 'side', 'high');
    add_meta_box('tdmagazine-page-post-template-settings', 'Page Template', 'tdmagazine_page_post_template_ui', 'page', 'side', 'high');
}
add_action( 'add_meta_boxes', 'tdmagazine_custom_metabox' );

/**
 * Options for Breaking News Meta Box
 *
 * @since tdmagazine 1.0
 */
 function tdmagazine_breaking_news_page_ui() {
    global $post;

    echo '<input type="hidden" name="tdmagazine_breaking_news_noncename" id="tdmagazine_breaking_news_noncename" value="' .
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

    $tdmagazine_breaking_news = get_post_meta( $post->ID, '_tdmagazine_breaking_news', true );

    echo '	<select name="_tdmagazine_breaking_news">
         		<option value="" ' . selected( $tdmagazine_breaking_news , '', false) . '>'.__( 'No', 'tdmagazine' ).'</option>
         		<option value="1" ' . selected( $tdmagazine_breaking_news , '1', false ) . '>'.__( 'Yes', 'tdmagazine' ).'</option>
         	</select>
         ';

}

/**
 * Options for Featured Post Meta Box
 *
 * @since tdmagazine 1.0
 */
 function tdmagazine_featured_post_page_ui() {
    global $post;

    echo '<input type="hidden" name="tdmagazine_featured_post_noncename" id="tdmagazine_featured_post_noncename" value="' .
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

    $tdmagazine_breaking_news = get_post_meta( $post->ID, '_tdmagazine_featured_post', true );

    echo '	<select name="_tdmagazine_featured_post">
         		<option value="" ' . selected( $tdmagazine_breaking_news , '', false) . '>'.__( 'No', 'tdmagazine' ).'</option>
         		<option value="1" ' . selected( $tdmagazine_breaking_news , '1', false ) . '>'.__( 'Yes', 'tdmagazine' ).'</option>
         	</select>
         ';

}

/**
 * Options for Page/Post Template Meta Box
 *
 * @since tdmagazine 1.0
 */
 function tdmagazine_page_post_template_ui() {
    global $post;

    wp_nonce_field( plugin_basename( __FILE__ ), 'tdmagazine_page_post_template_noncename' );

    $tdmagazine_page_template = get_post_meta( $post->ID, '_tdmagazine_page_post_template', true );

    echo '	<select name="_tdmagazine_page_post_template">
         		<option value="" ' . selected( $tdmagazine_page_template , '', false) . '>'.__( 'Right Sidebar', 'tdmagazine' ).'</option>
         		<option value="left-sidebar" ' . selected( $tdmagazine_page_template , 'left-sidebar', false ) . '>'.__( 'Left Sidebar', 'tdmagazine' ).'</option>
         		<option value="full-width" ' . selected( $tdmagazine_page_template , 'full-width', false ) . '>'.__( 'Full Width', 'tdmagazine' ).'</option>
         	</select>
         ';
}

/**
 *	Save Post Custom Metaboxes
 *	@since tdmagazine 1.0
 */
 function tdmagazine_save_custom_metabox( $post_id, $post ) {

    if ( isset( $_POST['post_type'] ) && $_POST['post_type'] == 'page' ) {
    	if ( !isset( $_POST['tdmagazine_page_post_template_noncename'] ) || !wp_verify_nonce( $_POST['tdmagazine_page_post_template_noncename'], plugin_basename(__FILE__) )) {
			return $post->ID;
		}
    } else {
    	if ( !isset( $_POST['tdmagazine_breaking_news_noncename'] ) || !wp_verify_nonce( $_POST['tdmagazine_breaking_news_noncename'], plugin_basename(__FILE__) )) {
			return $post->ID;
		} else if ( !isset( $_POST['tdmagazine_featured_post_noncename'] ) || !wp_verify_nonce( $_POST['tdmagazine_featured_post_noncename'], plugin_basename(__FILE__) )) {
			return $post->ID;
		} else if ( !isset( $_POST['tdmagazine_page_post_template_noncename'] ) || !wp_verify_nonce( $_POST['tdmagazine_page_post_template_noncename'], plugin_basename(__FILE__) )) {
			return $post->ID;
		}
    }

    if ( !current_user_can( 'edit_post', $post->ID )) {
    	return $post->ID;
    }

    $post_custom_data['_tdmagazine_breaking_news'] = isset( $_POST['_tdmagazine_breaking_news'] ) ? $_POST['_tdmagazine_breaking_news'] : '';
    $post_custom_data['_tdmagazine_featured_post'] = isset( $_POST['_tdmagazine_featured_post'] ) ? $_POST['_tdmagazine_featured_post'] : '';
    $post_custom_data['_tdmagazine_page_post_template'] = isset( $_POST['_tdmagazine_page_post_template'] ) ? $_POST['_tdmagazine_page_post_template'] : '';


    foreach ( $post_custom_data as $key => $value ) {
        if( $post->post_type == 'revision' ) return;

        $value = implode( ',', (array)$value );

        if( get_post_meta( $post->ID, $key, FALSE ) ) {
            update_post_meta( $post->ID, $key, $value );
        } else {
            add_post_meta( $post->ID, $key, $value );
        }

        if( !$value ) delete_post_meta( $post->ID, $key );
    }
}
 add_action('save_post', 'tdmagazine_save_custom_metabox', 1, 2);
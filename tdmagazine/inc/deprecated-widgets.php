<?php
/**
 * Deprecated Custom Widgets
 *
 * @package tdmagazine
 */

 /**
*  	Recent Posts Widget
*	@since tdmagazine 1.0
*/
class tdmagazine_recent_posts_widget extends WP_Widget {
	function __construct() {
		parent::__construct(false, $name = 'tdmagazine Recent Posts Widget', array( 'description' => 'This widget shows recent posts' ) );
	}

	function form( $instance ) {
		$tdmagazine_widget_title = isset( $instance['tdmagazine_widget_recent_posts_widget_title'] ) ? esc_attr( $instance['tdmagazine_widget_recent_posts_widget_title'] ) : '';
		$tdmagazine_widget_number = isset( $instance['tdmagazine_widget_recent_posts_widget_number'] ) ? esc_attr( $instance['tdmagazine_widget_recent_posts_widget_number'] ) : '5';
		$tdmagazine_widget_layout = isset( $instance['tdmagazine_widget_recent_posts_widget_layout'] ) ? esc_attr( $instance['tdmagazine_widget_recent_posts_widget_layout'] ) : 'thumb';
		$tdmagazine_widget_category = isset( $instance['tdmagazine_widget_recent_posts_widget_category'] ) ? esc_attr( $instance['tdmagazine_widget_recent_posts_widget_category'] ) : '';

		?>
			<p>This widget is deprecated and will be removed soon. Please use an alternative widget (Recent Posts).</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'tdmagazine_widget_recent_posts_widget_title' ); ?>"><?php _e( 'Title', 'tdmagazine' ); ?></label>
				<input id="<?php echo $this->get_field_id( 'tdmagazine_widget_recent_posts_widget_title' ); ?>" class="widefat" name="<?php echo $this->get_field_name( 'tdmagazine_widget_recent_posts_widget_title' ); ?>" type="text" value="<?php echo $tdmagazine_widget_title; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'tdmagazine_widget_recent_posts_widget_number' ); ?>"><?php _e( 'Number of posts to show:', 'tdmagazine' ); ?></label>
				<input id="<?php echo $this->get_field_id( 'tdmagazine_widget_recent_posts_widget_number' ); ?>" class="widefat" name="<?php echo $this->get_field_name( 'tdmagazine_widget_recent_posts_widget_number' ); ?>" type="text" value="<?php echo $tdmagazine_widget_number; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'tdmagazine_widget_recent_posts_widget_category' ); ?>"><?php _e( 'Category Name (Optional):', 'tdmagazine' ); ?></label>
				<input id="<?php echo $this->get_field_id( 'tdmagazine_widget_recent_posts_widget_category' ); ?>" class="widefat" name="<?php echo $this->get_field_name( 'tdmagazine_widget_recent_posts_widget_category' ); ?>" type="text" value="<?php echo $tdmagazine_widget_category; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'tdmagazine_widget_recent_posts_widget_layout' ); ?>"><?php _e( 'Image Width:', 'tdmagazine' ); ?></label>
				<select id="<?php echo $this->get_field_id( 'tdmagazine_widget_recent_posts_widget_layout' ); ?>" class="widefat" name="<?php echo $this->get_field_name( 'tdmagazine_widget_recent_posts_widget_layout' ); ?>">
					<option value="full" <?php selected($tdmagazine_widget_layout, 'full', true); ?>><?php _e( 'Full Size', 'tdmagazine' ); ?></option>
				  	<option value="thumb" <?php selected($tdmagazine_widget_layout, 'thumb', true); ?>><?php _e( 'Thumbnail', 'tdmagazine' ); ?></option>
				</select>
			</p>
		<?php

	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['tdmagazine_widget_recent_posts_widget_title'] = strip_tags( $new_instance['tdmagazine_widget_recent_posts_widget_title'] );
  		$instance['tdmagazine_widget_recent_posts_widget_number'] = intval(strip_tags( $new_instance['tdmagazine_widget_recent_posts_widget_number'] ));
  		$instance['tdmagazine_widget_recent_posts_widget_layout'] = strip_tags( $new_instance['tdmagazine_widget_recent_posts_widget_layout'] );
  		$instance['tdmagazine_widget_recent_posts_widget_category'] = strip_tags( $new_instance['tdmagazine_widget_recent_posts_widget_category'] );

    	return $instance;
	}

	function widget( $args, $instance ) {

		if( $instance['tdmagazine_widget_recent_posts_widget_category'] != '' ) {
			$category_id = get_cat_ID( $instance['tdmagazine_widget_recent_posts_widget_category'] );
			$recent_posts_args = array(
				'post_type' => 'post',
    			'posts_per_page' => $instance['tdmagazine_widget_recent_posts_widget_number'],
    			'ignore_sticky_posts' => 1,
    			'cat' => $category_id
			);
		} else {
			$recent_posts_args = array(
				'post_type' => 'post',
    			'posts_per_page' => $instance['tdmagazine_widget_recent_posts_widget_number'],
    			'ignore_sticky_posts' => 1
			);
		}

		$recent_posts_query = new WP_Query( $recent_posts_args );
		$image_width = $instance['tdmagazine_widget_recent_posts_widget_layout'];

		echo $args['before_widget'];
		echo '<h3 class="widget-title">'.$instance['tdmagazine_widget_recent_posts_widget_title'].'</h3>';

		if( $recent_posts_query->have_posts() ) {
			$recent_posts_output = '<ul>';
			while( $recent_posts_query->have_posts() ) {
				$recent_posts_query->the_post();
				$recent_posts_output .= '<li>';

				if( $image_width === 'full' ) {
					$recent_posts_output .= '<div class="recent-post-entry-image" style="float: none; margin: 0; width: 100%;"><a href="'.get_permalink().'" rel="bookmark" title="'.get_the_title().'">'.get_the_post_thumbnail().'</a></div>';
				} else {
					$recent_posts_output .= '<div class="recent-post-entry-image"><a href="'.get_permalink().'" rel="bookmark" title="'.get_the_title().'">'.get_the_post_thumbnail( $recent_posts_query->post->ID, array( 46, 46 ) ).'</a></div>';
				}

				$recent_posts_output .= '<h5 class="recent-post-entry-title"><a href="'.get_permalink().'" rel="bookmark" title="'.get_the_title().'">'.get_the_title().'</a></h5>';
				//$recent_posts_output .= '<span class="recent-post-date">'.esc_html( strftime('%B %e, %Y', get_post_time('U', true)) ). '</span>';
				$recent_posts_output .= '<span class="recent-post-date">'.esc_html( get_the_date() ). '</span>';
				$recent_posts_output .= '</li>';
			}
			$recent_posts_output .= '</ul>';

			echo $recent_posts_output;
		}
		wp_reset_query();
		echo $args['after_widget'];
	}

}

/**
*  	Popular Posts Widget
*	@since tdmagazine 1.0
*/
class tdmagazine_popular_posts_widget extends WP_Widget {
	function __construct() {
		parent::__construct(false, $name = 'tdmagazine Popular Posts Widget', array( 'description' => 'This widget shows popular posts' ) );
	}

	function form( $instance ) {
		$tdmagazine_widget_title = isset( $instance['tdmagazine_widget_popular_posts_widget_title'] ) ? esc_attr( $instance['tdmagazine_widget_popular_posts_widget_title'] ) : '';
		$tdmagazine_widget_number = isset( $instance['tdmagazine_widget_popular_posts_widget_number'] ) ? esc_attr( $instance['tdmagazine_widget_popular_posts_widget_number'] ) : '5';
		$tdmagazine_widget_layout = isset( $instance['tdmagazine_widget_popular_posts_widget_layout'] ) ? esc_attr( $instance['tdmagazine_widget_popular_posts_widget_layout'] ) : 'thumb';
		$tdmagazine_widget_category = isset( $instance['tdmagazine_widget_popular_posts_widget_category'] ) ? esc_attr( $instance['tdmagazine_widget_popular_posts_widget_category'] ) : '';

		?>
			<p>This widget is deprecated and will be removed soon. Please use an alternative widget (Recent Posts).</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'tdmagazine_widget_popular_posts_widget_title' ); ?>"><?php _e( 'Title', 'tdmagazine' ); ?></label>
				<input id="<?php echo $this->get_field_id( 'tdmagazine_widget_popular_posts_widget_title' ); ?>" class="widefat" name="<?php echo $this->get_field_name( 'tdmagazine_widget_popular_posts_widget_title' ); ?>" type="text" value="<?php echo $tdmagazine_widget_title; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'tdmagazine_widget_popular_posts_widget_number' ); ?>"><?php _e( 'Number of posts to show:', 'tdmagazine' ); ?></label>
				<input id="<?php echo $this->get_field_id( 'tdmagazine_widget_popular_posts_widget_number' ); ?>" class="widefat" name="<?php echo $this->get_field_name( 'tdmagazine_widget_popular_posts_widget_number' ); ?>" type="text" value="<?php echo $tdmagazine_widget_number; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'tdmagazine_widget_popular_posts_widget_category' ); ?>"><?php _e( 'Category Name (Optional):', 'tdmagazine' ); ?></label>
				<input id="<?php echo $this->get_field_id( 'tdmagazine_widget_popular_posts_widget_category' ); ?>" class="widefat" name="<?php echo $this->get_field_name( 'tdmagazine_widget_popular_posts_widget_category' ); ?>" type="text" value="<?php echo $tdmagazine_widget_category; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'tdmagazine_widget_popular_posts_widget_layout' ); ?>"><?php _e( 'Image Width:', 'tdmagazine' ); ?></label>
				<select id="<?php echo $this->get_field_id( 'tdmagazine_widget_popular_posts_widget_layout' ); ?>" class="widefat" name="<?php echo $this->get_field_name( 'tdmagazine_widget_popular_posts_widget_layout' ); ?>">
					<option value="full" <?php selected($tdmagazine_widget_layout, 'full', true); ?>><?php _e( 'Full Size', 'tdmagazine' ); ?></option>
				  	<option value="thumb" <?php selected($tdmagazine_widget_layout, 'thumb', true); ?>><?php _e( 'Thumbnail', 'tdmagazine' ); ?></option>
				</select>
			</p>
		<?php

	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['tdmagazine_widget_popular_posts_widget_title'] = strip_tags( $new_instance['tdmagazine_widget_popular_posts_widget_title'] );
  		$instance['tdmagazine_widget_popular_posts_widget_number'] = intval(strip_tags( $new_instance['tdmagazine_widget_popular_posts_widget_number'] ));
  		$instance['tdmagazine_widget_popular_posts_widget_layout'] = strip_tags( $new_instance['tdmagazine_widget_popular_posts_widget_layout'] );
  		$instance['tdmagazine_widget_popular_posts_widget_category'] = strip_tags( $new_instance['tdmagazine_widget_popular_posts_widget_category'] );

    	return $instance;
	}

	function widget( $args, $instance ) {
		//setlocale( LC_TIME, get_locale() );

		if( $instance['tdmagazine_widget_popular_posts_widget_category'] != '' ) {
			$category_id = get_cat_ID( $instance['tdmagazine_widget_popular_posts_widget_category'] );
			$recent_posts_args = array(
				'post_type' => 'post',
    			'posts_per_page' => $instance['tdmagazine_widget_popular_posts_widget_number'],
    			'ignore_sticky_posts' => 1,
    			'orderby'=>'comment_count',
    			'order' => 'DESC',
    			'cat' => $category_id
			);
		} else {
			$recent_posts_args = array(
				'post_type' => 'post',
    			'posts_per_page' => $instance['tdmagazine_widget_popular_posts_widget_number'],
    			'ignore_sticky_posts' => 1,
    			'orderby'=>'comment_count',
    			'order' => 'DESC'
			);
		}

		$recent_posts_query = new WP_Query( $recent_posts_args );
		$image_width = $instance['tdmagazine_widget_popular_posts_widget_layout'];

		echo $args['before_widget'];
		echo '<h3 class="widget-title">'.$instance['tdmagazine_widget_popular_posts_widget_title'].'</h3>';

		if( $recent_posts_query->have_posts() ) {
			$recent_posts_output = '<ul>';
			while( $recent_posts_query->have_posts() ) {
				$recent_posts_query->the_post();
				$recent_posts_output .= '<li>';

				if( $image_width === 'full' ) {
					$recent_posts_output .= '<div class="popular-post-entry-image" style="float: none; margin: 0; width: 100%;"><a href="'.get_permalink().'" rel="bookmark" title="'.get_the_title().'">'.get_the_post_thumbnail().'</a></div>';
				} else {
					$recent_posts_output .= '<div class="popular-post-entry-image"><a href="'.get_permalink().'" rel="bookmark" title="'.get_the_title().'">'.get_the_post_thumbnail( $recent_posts_query->post->ID, array( 46, 46 ) ).'</a></div>';
				}

				$recent_posts_output .= '<h5 class="popular-post-entry-title"><a href="'.get_permalink().'" rel="bookmark" title="'.get_the_title().'">'.get_the_title().'</a></h5>';
				//$recent_posts_output .= '<span class="popular-post-date">'.esc_html( strftime('%B %e, %Y', get_post_time('U', true)) ). '</span>';
				$recent_posts_output .= '<span class="popular-post-date">'.esc_html( get_the_date() ). '</span>';
				$recent_posts_output .= '</li>';
			}
			$recent_posts_output .= '</ul>';

			echo $recent_posts_output;
		}
		wp_reset_query();
		echo $args['after_widget'];
	}

}


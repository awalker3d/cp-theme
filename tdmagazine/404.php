<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package tdmagazine
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<article id="post-0" class="post not-found hentry">
				<header class="entry-header">
					<h2 class="entry-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'tdmagazine' ); ?></h2>
				</header><!-- .entry-header -->

				<div class="entry-content">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'tdmagazine' ); ?></p>

					<?php get_search_form(); ?>

					<div id="archive-404" class="row">

						<div class="custom-archive-item col-lg-4 col-md-4">
							<h3 class="section-title"><?php _e( 'Latest Posts', 'tdmagazine' ); ?></h3>

							<ul class="section-list">
								<?php
									$args = array( 'numberposts' => '10', 'post_status' => 'publish' );
									$recent_posts = wp_get_recent_posts( $args );
									foreach( $recent_posts as $recent ){
										echo '<li><a href="' . get_permalink( $recent["ID"] ) . '" title="'.esc_attr( $recent["post_title"] ).'" >' . $recent["post_title"].'</a> </li> ';
									}
								?>
							</ul><!-- .section-list -->
						</div><!-- .custom-archive-item -->

						<div class="custom-archive-item col-lg-4 col-md-4">
							<h3 class="section-title"><?php _e( 'Archives by Month', 'tdmagazine' ); ?></h3>
							<ul class="section-list">
								<?php wp_get_archives( 'type=monthly' ); ?>
							</ul><!-- .section-list -->
						</div><!-- .custom-archive-item -->

						<div class="custom-archive-item col-lg-4 col-md-4">
							<h3 class="section-title"><?php _e( 'Archive By Year', 'tdmagazine' ); ?></h3>
							<ul class="section-list">
								<?php wp_get_archives( 'type=yearly' ); ?>
							</ul><!-- .section-list -->
						</div><!-- .custom-archive-item -->

						<div class="custom-archive-item col-lg-4 col-md-4">
							<h3 class="section-title"><?php _e( 'Pages', 'tdmagazine' ); ?></h3>
							<ul class="section-list">
							 <?php
								$pages = get_pages();
								foreach ( $pages as $page ) {
									echo '<li><a href="'.get_page_link( $page->ID ).'">'.$page->post_title.'</a></li>';
								  }
							 ?>
							</ul><!-- .section-list -->
						</div><!-- .custom-archive-item -->

						<div class="custom-archive-item col-lg-4 col-md-4">
							<h3 class="section-title"><?php _e( 'Categories', 'tdmagazine' ); ?></h3>
							<ul class="section-list">
							<?php
								$args = array(
									'orderby' => 'name',
									'order' => 'ASC'
								);

								$categories = get_categories( $args );
								foreach( $categories as $category ) {
									echo '<li><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s", "tdmagazine" ), $category->name ) . '" ' . '>' . $category->name.'</a> ('.$category->count.') </li>';
								}
							?>
							</ul><!-- .section-list -->
						</div><!-- .custom-archive-item -->

						<div class="custom-archive-item col-lg-4 col-md-4">
							<h3 class="section-title"><?php _e( 'Contributors', 'tdmagazine' ); ?></h3>
							<ul class="section-list">
							<?php
								$args = array(
									'show_fullname' => true,
									'optioncount' => true,
									'orderby' => 'post_count',
									'order' => 'DESC'
								);

								wp_list_authors($args);
							?>
							</ul><!-- .section-list -->
						</div><!-- .custom-archive-item -->
					</div><!-- .row -->

				</div><!-- .entry-content -->
			</article><!-- #post-0 .post .not-found -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
/*! http://tinynav.viljamis.com v1.1 by @viljamis */
(function(a,i,g){a.fn.tinyNav=function(j){var b=a.extend({active:"selected",header:"",label:""},j);return this.each(function(){g++;var h=a(this),d="tinynav"+g,f=".l_"+d,e=a("<select/>").attr("id",d).addClass("tinynav "+d);if(h.is("ul,ol")){""!==b.header&&e.append(a("<option/>").text(b.header));var c="";h.addClass("l_"+d).find("a").each(function(){c+='<option value="'+a(this).attr("href")+'">';var b;for(b=0;b<a(this).parents("ul, ol").length-1;b++)c+="- ";c+=a(this).text()+"</option>"});e.append(c);
b.header||e.find(":eq("+a(f+" li").index(a(f+" li."+b.active))+")").attr("selected",!0);e.change(function(){i.location.href=a(this).val()});a(f).after(e);b.label&&e.before(a("<label/>").attr("for",d).addClass("tinynav_label "+d+"_label").append(b.label))}})}})(jQuery,this,0);


jQuery(document).ready(function(){
	jQuery(".hentry, .widget").fitVids();

	//Social Links meta
	var socialLinks = jQuery('.social-list');
	if( socialLinks.length ) {
		socialLinks.find( '.social-meta' ).each(function() {
			var currentSocialLink = jQuery(this);
			currentSocialLink.parent().attr( 'title', currentSocialLink.text() ).addClass('td-tooltip');
		});
	}
	//Tooltip
	jQuery('.td-tooltip').tooltipster({position: 'top'});

	//Mobile Menu
	jQuery(function(){
		jQuery('.top-section .nav-bar').tinyNav({header: jQuery('.main-navigation').data('small-nav-title')});

		jQuery(".main-navigation .nav-bar").slicknav({
			prependTo:'#mobile-site-navigation',
			allowParentLinks: true,
			label: jQuery('#site-navigation').data('small-nav-title')
      	});
	});

	//Blog layout
	jQuery(function() {
		jQuery('.content-grid').imagesLoaded(function(){
            jQuery('.content-grid').isotope({
                itemSelector : '.post-box'
            });
        });
	});

	//404 Archive layout
	jQuery(function() {
		if( jQuery('#archive-404').length ) {
			jQuery('#archive-404').isotope({
                itemSelector : '.custom-archive-item'
            });
		}
	});


	//Share buttons
	jQuery(function(){
    	if( jQuery('.share-section').length ) {
    		jQuery('a.facebook, a.twitter, a.googleplus, a.linkedin, a.pinterest').on('click', function(e) {
    			e.preventDefault();
    			var left  = (jQuery(window).width()/2)-(900/2),
					top   = (jQuery(window).height()/2)-(600/2);
    			var newwindow=window.open(jQuery(this).attr('href'),'',"height=400,width=600,top="+top+", left="+left+"");
        		if (window.focus) {newwindow.focus()}
    		});
    	}
    });

	if( tdmagazineParams.backgroundImages && jQuery.fn.backstretch ) {
		jQuery.backstretch(JSON.parse(tdmagazineParams.backgroundImages), {duration: 5500, fade: 750});
	}

	jQuery(function() {
		if ( tdmagazineParams.isBreakingNews ) {
			jQuery('#breaking-news-tems').ticker({
				 controls: false,
				 debugMode: false,
				 titleText: jQuery('#breaking-news-tems').data('breaking-news-title')
			});
		}
	});

	jQuery(function() {
		var sliderContent = jQuery('.hentry .bxslider');
		var currentGrid = jQuery('.content-grid');
		sliderContent.imagesLoaded(function() {
			sliderContent.bxSlider({
			  mode: 'fade',
			  pager: false,
			  adaptiveHeight: true,
			  auto: false,
			  autoHover: true,
			  pause: 7000,
			  prevText: '<i class="fa fa-chevron-left"></i>',
			  nextText: '<i class="fa fa-chevron-right"></i>',
			  onSliderLoad: function() {
					sliderContent.css("visibility", "visible").fadeTo(200, 1);
					currentGrid.isotope();
					currentGrid.isotope('layout');
				},
				onSlideAfter: function() {
					currentGrid.isotope();
					currentGrid.isotope('layout');
				}
			});
		});
	});

	jQuery(function() {
		if( tdmagazineParams.isFixedMenu ) {
			var stickyNavigationContainer = jQuery('.main-navigation');
			var stickyNavigationTop = stickyNavigationContainer.offset().top;

			var stickyNavigation = function() {
				var scrollTop = jQuery(window).scrollTop();

				if (scrollTop > stickyNavigationTop) {
					stickyNavigationContainer.addClass('sticky-navigation');
				} else {
					stickyNavigationContainer.removeClass('sticky-navigation');
				}
			};

			stickyNavigation();

			jQuery(window).scroll(function() {
				stickyNavigation();
			});

		}
	});

	var bodyContainer = jQuery('body');
	smallScreen = function() {
		var browserWidth = jQuery( window ).width();

		if( browserWidth < 979 && browserWidth > 768 ) {
			bodyContainer.addClass( 'default-site-content' );
		} else {
			bodyContainer.removeClass( 'default-site-content' );
		}

		if( browserWidth < 767 && browserWidth > 480) {
			bodyContainer.addClass( 'tablet-site-content' );
		} else {
			bodyContainer.removeClass( 'tablet-site-content' );
		}

		if( browserWidth < 480) {
			bodyContainer.addClass( 'phone-site-content' );
		} else {
			bodyContainer.removeClass( 'phone-site-content' );
		}

		if ( jQuery( window ).width() < 760 ) {
			jQuery('.nav-bar').hide();
			jQuery('#tinynav1, #tinynav2').show();
		} else {
			jQuery('.nav-bar').show();
			jQuery('#tinynav1, #tinynav2').hide();
		}
	}

	jQuery(window).resize(function() {
		smallScreen();
	});

	jQuery(window).scroll(function() {
		if(jQuery(this).scrollTop() > 300) {
			jQuery('#gotop').fadeIn();
		} else {
			jQuery('#gotop').fadeOut();
		}
	});

	jQuery('#gotop').click(function () {
		jQuery('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
});
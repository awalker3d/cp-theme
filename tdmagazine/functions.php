<?php
/**
 * tdmagazine functions and definitions
 *
 * @package tdmagazine
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

if ( ! function_exists( 'tdmagazine_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function tdmagazine_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on tdmagazine, use a find and replace
	 * to change 'tdmagazine' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'tdmagazine', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	 register_nav_menus( array(
		'top-menu' => __( 'Top Menu', 'tdmagazine' ),
		'main-menu' => __( 'Main Menu', 'tdmagazine' ),
		'social' => __( 'Social Menu', 'tdmagazine' ),
	 ) );

	 /*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'gallery', 'video' ) );

	if ( function_exists( 'add_image_size' ) ) {
		add_image_size( 'td-medium-thumb', 660, 440, true );
		add_image_size( 'standard-image', 800, 560, true );
		add_image_size( 'standard-image-wide', 1170, 560, true );
		add_image_size( 'thumb-image', 220, 220, true );
	}
}
endif; // tdmagazine_setup
add_action( 'after_setup_theme', 'tdmagazine_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function tdmagazine_widgets_init() {

	/* Custom Widgets */
	require get_template_directory() . '/inc/widgets.php';
	register_widget( 'tdmagazine_recent_comments_widget' );
	register_widget( 'tdmagazine_author_widget' );
	register_widget( 'tdmagazine_login_widget' );
	register_widget( 'tdmagazine_newsletter_widget' );
	register_widget( 'tdmagazine_rp_standard_widget' );
	register_widget( 'tdmagazine_rp_columns_widget' );

	/* Deprecated Custom Widgets */
	require get_template_directory() . '/inc/deprecated-widgets.php';
	register_widget( 'tdmagazine_recent_posts_widget' );
	register_widget( 'tdmagazine_popular_posts_widget' );

	register_sidebar( array(
		'name'          => __( 'Homepage', 'tdmagazine' ),
		'id'            => 'homepage-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title accent-color">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => __( 'Sidebar', 'tdmagazine' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title accent-color">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer #1', 'tdmagazine' ),
		'id'            => 'sidebar-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer #2', 'tdmagazine' ),
		'id'            => 'sidebar-3',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer #3', 'tdmagazine' ),
		'id'            => 'sidebar-4',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer #4', 'tdmagazine' ),
		'id'            => 'sidebar-5',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'tdmagazine_widgets_init' );

/**
 * Register Google Fonts
 */
function tdmagazine_google_fonts() {
	$fonts_url = '';

	$roboto_font = _x( 'on', 'Roboto font: on or off', 'tdmagazine' );
	$roboto_slab_font =  _x( 'on', 'Roboto Slab font: on or off', 'tdmagazine' );

	if ( 'off' !== $roboto_font || 'off' !== $roboto_slab_font ) {
		$font_families = array();

		if ( 'off' !== $roboto_font ) {
			$font_families[] = 'Roboto:400,700';
		}

		if ( 'off' !== $roboto_slab_font ) {
			$font_families[] = 'Roboto Slab:400,700';
		}

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext,cyrillic,cyrillic-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, "//fonts.googleapis.com/css" );
	}

	return $fonts_url;
}

/**
 * Enqueue scripts and styles
 */
function tdmagazine_scripts() {
	wp_enqueue_style( 'tdmagazine-fonts', tdmagazine_google_fonts(), array(), null );

	wp_enqueue_style( 'tdmagazine-css-framework', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'tdmagazine-icons', get_template_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'tdmagazine-style', get_stylesheet_uri() );

	wp_enqueue_script( 'tdmagazine-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'tdmagazine-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}

	wp_enqueue_script( 'tdmagazine-assets', get_template_directory_uri() . '/js/jquery.assets.js', array( 'jquery' ), '201401', true  );
	wp_enqueue_script( 'tdmagazine-script', get_template_directory_uri() . '/js/tdmagazine.js', array( 'jquery' ), '201301' );

	$tdmagazine_params = array(
		'isBreakingNews' => tdmagazine_is_breaking_news(),
		'isFixedMenu' => tdmagazine_is_fixed_menu(),
		'backgroundImages' => tdmagazine_is_background_image()
	);

	wp_localize_script( 'tdmagazine-script', 'tdmagazineParams', $tdmagazine_params );

}
add_action( 'wp_enqueue_scripts', 'tdmagazine_scripts' );

/**
 *	Remove text after comment textarea
 *	@since tdmagazine 1.0
 */
function tdmagazine_remove_comment_styling_prompt($defaults) {
	$defaults['comment_notes_after'] = '';
	return $defaults;
}
add_filter('comment_form_defaults', 'tdmagazine_remove_comment_styling_prompt');

/**
*	Add Paragraphs to the Bio
*	@since tdmagazine 1.2.3
*/
function tdmagazine_user_bio( $content ){
	return nl2br( $content );
}
add_filter('the_author_description', 'tdmagazine_user_bio', 1, 1);

/**
 *	Customize excerpts more tag
 *	@since tdmagazine 1.0
 */
function tdmagazine_excerpt_more($more) {
    global $post;
	return '... <a class="more-link accent-color" href="'. get_permalink($post->ID) . '">'. __( 'Continue reading', 'tdmagazine' ) .'</a>';
}
add_filter('excerpt_more', 'tdmagazine_excerpt_more');

/**
 *	Change Excerpt Length
 *	@since tdmagazine 1.2.1
 */
function tdmagazine_excerpt_length( $length ) {
	return tdmagazine_auto_summary_length();
}
add_filter( 'excerpt_length', 'tdmagazine_excerpt_length', 999 );

/**
*	Add custom style/script to website head
*	@since tdmagazine 1.0
*/
function tdmagazine_head() {
	echo "<!--[if lt IE 9]><script src='".get_template_directory_uri()."/js/html5.js'></script><![endif]--> \n";
	tdmagazine_custom_styles();
}
add_action('wp_head', 'tdmagazine_head');

/**
 * Add an ad to the beginning of every post page.
 *
 * @uses is_single()
 */
function tdmagazine_content_filter_ad_section( $content ) {
	$entry_ad = get_theme_mod( 'tdmagazine_ads_post_entry_ad', '' );
    if ( is_single() && !is_attachment() && $entry_ad != '' ) {
    	$output = '<div id="entry-ad-section">';
		$output .= '<p class="ad-title">'.__( 'Advertisement', 'tdmagazine' ).'</p>';
		$output .= $entry_ad;
		$output .= '</div>';
        $content = $output.$content;
    }

    return $content;
}
add_filter( 'the_content', 'tdmagazine_content_filter_ad_section', 20 );

/**
 *	Add Custom Social Fields to User Profile Page
 *	@since tdmagazine 1.1
 */
function tdmagazine_custom_contact_fields( $profile_fields ) {
	$profile_fields['twitter'] = __( 'Twitter Username', 'tdmagazine' );
	$profile_fields['facebook'] = __( 'Facebook URL', 'tdmagazine' );
	$profile_fields['gplus'] = __( 'Google+ URL', 'tdmagazine' );
	$profile_fields['linkedin'] = __( 'LinkedIn URL', 'tdmagazine' );
	$profile_fields['instagram'] = __( 'Instagram URL', 'tdmagazine' );

	return $profile_fields;
}
add_filter('user_contactmethods', 'tdmagazine_custom_contact_fields');

/**
 *	Highlight Custom widgets
 *	@since tdmagazine 2.0
 */
function tdmagazine_admin_css(){ ?>
	<style type="text/css">
	div.widget[id*=_tdmagazine_] .widget-top {
    	background: #2980b9;
    	color: #fff;
	}

	div.widget[id*=_tdmagazine_] .in-widget-title {
		color: #e2e3e3;
	}
	</style>
<?php
}
add_action( 'admin_head', 'tdmagazine_admin_css' );

/**
 *	Remove Gallery Inline Styling
 *	@since tdmagazine 2.0
 */
add_filter( 'use_default_gallery_style', '__return_false' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom colors.
 */
require get_template_directory() . '/inc/custom-colors.php';

/**
 * Site breadcrumb
 */
require get_template_directory() . '/inc/breadcrumb.php';

/**
 * Share Buttons
 */
require get_template_directory() . '/inc/share-buttons.php';

/**
 * Post meta
 */
require get_template_directory() . '/inc/post-meta.php';

/**
 * Deprecated functions
 */
require get_template_directory() . '/inc/deprecated-functions.php';

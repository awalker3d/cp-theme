<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package tdmagazine
 */

get_header(); ?>

<div class="row">
	<div id="primary" class="content-area <?php echo esc_attr( tdmagazine_get_blog_primary_class() ); ?>">
		<div id="content" class="site-content" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">
					<?php
						if ( is_category() ) :
							single_cat_title();

						elseif ( is_tag() ) :
							single_tag_title();

						elseif ( is_author() ) :
							printf( __( 'Author: %s', 'tdmagazine' ), '<span class="vcard">' . get_the_author() . '</span>' );

						elseif ( is_day() ) :
							printf( __( 'Day: %s', 'tdmagazine' ), '<span>' . get_the_date() . '</span>' );

						elseif ( is_month() ) :
							printf( __( 'Month: %s', 'tdmagazine' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'tdmagazine' ) ) . '</span>' );

						elseif ( is_year() ) :
							printf( __( 'Year: %s', 'tdmagazine' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'tdmagazine' ) ) . '</span>' );

						elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
							_e( 'Asides', 'tdmagazine' );

						elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
							_e( 'Galleries', 'tdmagazine');

						elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
							_e( 'Images', 'tdmagazine');

						elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
							_e( 'Videos', 'tdmagazine' );

						elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
							_e( 'Quotes', 'tdmagazine' );

						elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
							_e( 'Links', 'tdmagazine' );

						elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
							_e( 'Statuses', 'tdmagazine' );

						elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
							_e( 'Audios', 'tdmagazine' );

						elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
							_e( 'Chats', 'tdmagazine' );

						else :
							_e( 'Archives', 'tdmagazine' );

						endif;
					?>
				</h1>
				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
			</header><!-- .page-header -->

			<?php if( tdmagazine_is_two_columns_layout() ): ?>
			<div class="content-grid row columns-blog">

				<?php while ( have_posts() ) : the_post(); ?>
					<div class="col-lg-6 col-md-6 post-box">
					<?php get_template_part( 'content', get_post_format() ); ?>
					</div><!-- .post-box -->
				<?php endwhile; ?>

			</div><!-- .content-grid -->
			<?php elseif( tdmagazine_is_three_columns_layout() ): ?>
			<div class="content-grid row columns-blog">

				<?php while ( have_posts() ) : the_post(); ?>
					<div class="col-lg-4 col-md-4 post-box">
					<?php get_template_part( 'content', get_post_format() ); ?>
					</div><!-- .post-box -->
				<?php endwhile; ?>

			</div><!-- .content-grid -->
			<?php elseif( tdmagazine_is_inline_layout() ): ?>

				<div class="inline-blog">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'inline' ); ?>
				<?php endwhile; ?>
				</div><!-- .inline-blog -->

			<?php else: ?>

				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', get_post_format() ); ?>
				<?php endwhile; ?>

			<?php endif; ?>

			<?php tdmagazine_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

	<?php if( !tdmagazine_is_sidebar_hidden() ): ?>
	<div class="col-lg-4 col-md-4 sidebar-container">
		<?php get_sidebar(); ?>
	</div><!-- .sidebar-container -->
	<?php endif; ?>

</div><!-- .row -->

<?php get_footer(); ?>
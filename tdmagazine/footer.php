<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package tdmagazine
 */
?>

		<?php tdmagazine_ad_section( 'bottom' ); ?>

	</div><!-- #main -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div id="footer-widgets" class="site-info row">
			<div class="col-lg-3 col-md-3">
				<?php dynamic_sidebar( 'sidebar-2' ); ?>
			</div><!-- .col -->
			<div class="col-lg-3 col-md-3">
				<?php dynamic_sidebar( 'sidebar-3' ); ?>
			</div><!-- .col -->
			<div class="col-lg-3 col-md-3">
				<?php dynamic_sidebar( 'sidebar-4' ); ?>
			</div><!-- .col -->
			<div class="col-lg-3 col-md-3">
				<?php dynamic_sidebar( 'sidebar-5' ); ?>
			</div><!-- .col -->
		</div><!-- .site-info -->

		<div class="footer-bottom clearfix">
			<div class="c pull-left">
				<?php _e('Copyright', 'tdmagazine'); ?> <?php echo the_date('Y'); ?> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> | <?php printf( __( '%1$s by %2$s', 'tdmagazine' ), tdmagazine_affiliate_link(), '<a href="http://tdwp.us/" target="_blank" rel="designer">(td)WP</a>' ); ?>
			</div><!-- .c -->

			<?php if( tdmagazine_is_social_bottom() ): ?>
			<div class="pull-right footer-social">
				<?php get_template_part( 'menu', 'social' ); ?>
			</div><!-- .footer-social -->
			<?php endif; ?>
		</div><!-- .footer-bottom -->
	</footer><!-- #colophon -->

	<div id="gotop"><i class="fa fa-chevron-up"></i></div><!-- #gotop -->

	<div><!-- .site-inner -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
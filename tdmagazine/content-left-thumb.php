<?php
/**
 * @package tdmagazine
 */
?>
<?php $content_span = 'span12'; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="hentry-inner">
		<div class="row-fluid">
			<?php if ( has_post_thumbnail() ): ?>
			<?php $content_span = 'span6'; ?>
			<div class="span6">
				<div class="post-thumb">
					<a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>">
					<?php the_post_thumbnail('td-medium-thumb'); ?>
					</a>
				</div><!-- .post-thumb -->
			</div><!-- .span -->
			<?php endif; ?>
			
			<div class="<?php echo $content_span; ?>">
				<header class="entry-header">
					<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					<?php if ( 'post' == get_post_type() ) : ?>
					<div class="entry-meta top clearfix">
						<?php tdmagazine_posted_on(); ?>
					</div><!-- .entry-meta -->
					<?php endif; ?>
				</header><!-- .entry-header -->

				<?php if ( is_search() ) : ?>
				<div class="entry-summary">
					<?php the_excerpt(); ?>
				</div><!-- .entry-summary -->
				<?php else : ?>
				<div class="entry-content">
					<?php if( tdmagazine_is_auto_summary() ): ?>
						<?php the_excerpt(); ?> 
					<?php else: ?>	
						<?php the_content( __( '', 'tdmagazine' ) ); ?>
					<?php endif; ?>
			
					<?php
						wp_link_pages( array(
							'before' => '<div class="page-links">' . __( 'Pages:', 'tdmagazine' ),
							'after'  => '</div>',
						) );
					?>
				</div><!-- .entry-content -->
				<?php endif; ?>
			</div><!-- span -->
		</div><!-- .row-fluid -->
		<footer class="entry-meta bottom">
			<div class="read-more-container">
				<a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>"><?php _e( 'Read More', 'tdmagazine' ); ?> <i class="fa fa-chevron-circle-right"></i></a>
			</div><!-- .read-more-container -->
		</footer><!-- .entry-meta -->
	</div><!-- .hentry-inner -->
</article><!-- #post-## -->

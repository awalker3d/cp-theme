<?php
/**
 * @package tdmagazine
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( has_post_thumbnail() ): ?>
	<div class="post-thumb">
		<a href="<?php echo esc_url( get_permalink() ); ?>" class="thumb-link">
		<?php the_post_thumbnail( 'standard-image' ); ?>
		</a><!-- .thumb-link -->
	</div><!-- .post-thumb -->
	<?php endif; ?>

	<header class="entry-header">
		<h2 class="entry-title"><a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta top clearfix">
			<?php tdmagazine_posted_on(); ?>
			<?php edit_post_link( __( 'Edit', 'tdmagazine' ), '<span class="edit-link"> / ', '</span>' ); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->


	<div class="entry-content">
		<?php if( tdmagazine_is_auto_summary() ): ?>
			<?php the_excerpt(); ?>
		<?php else: ?>
			<?php the_content( '' ); ?>
		<?php endif; ?>

		<?php
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'tdmagazine' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>'
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-meta bottom">
		<div class="read-more-container">
			<a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>"><?php _e( 'Read More', 'tdmagazine' ); ?> <i class="fa fa-chevron-circle-right"></i></a>
		</div><!-- .read-more-container -->
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->

<?php
  /*
  Template Name: Home Page
  */
?>

<?php get_header(); ?>

<div class="row">
	<div id="primary" class="content-area <?php echo esc_attr( tdmagazine_get_blog_primary_class() ); ?>">
		<div class="featured-container">
			<?php tdmagazine_featured_posts(); ?>
		</div><!-- .featured-container -->
		<div id="homepage-widgets">
		<?php
			if( is_active_sidebar( 'homepage-1' ) ) {
				dynamic_sidebar( 'homepage-1' );
			}
		?>
		</div><!-- #homepage-widgets -->
	</div><!-- #primary -->

	<?php if( !tdmagazine_is_sidebar_hidden() ): ?>
	<div class="col-lg-4 col-md-4 sidebar-container">
		<?php get_sidebar(); ?>
	</div><!-- .sidebar-container -->
	<?php endif; ?>

</div><!-- .row -->

<?php get_footer(); ?>
<?php
  /*
  Template Name: Archive Page
  */
?>

<?php get_header(); ?>

<div class="row">
	<div id="primary" class="content-area <?php echo esc_attr( tdmagazine_get_blog_primary_class() ); ?>">
		<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('custom-archive-hentry'); ?>>

				<header class="entry-header">
							<?php if ( has_post_thumbnail() ): ?>
							<div class="post-thumb">
								<?php the_post_thumbnail(); ?>
							</div><!-- .post-thumb -->
							<?php endif; ?>

							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</header><!-- .entry-header -->

						<div class="entry-content">
							<?php the_content(); ?>
						</div><!-- .entry-content -->

						<div class="custom-archive-container">
							<div class="custom-archive-item">
								<h3 class="section-title"><?php _e( 'Latest Posts', 'tdmagazine' ); ?></h3>

								<ul class="section-list">
									<?php
										$args = array( 'numberposts' => '15', 'post_status' => 'publish' );
										$recent_posts = wp_get_recent_posts( $args );
										foreach( $recent_posts as $recent ){
											echo '<li><a href="' . get_permalink( $recent["ID"] ) . '" title="'.esc_attr( $recent["post_title"] ).'" >' . $recent["post_title"].'</a> </li> ';
										}
									?>
								</ul><!-- .section-list -->
							</div><!-- .custom-archive-item -->

							<div class="custom-archive-item">
								<h3 class="section-title"><?php _e( 'Archives by Month', 'tdmagazine' ); ?></h3>
								<ul class="section-list">
									<?php wp_get_archives( 'type=monthly' ); ?>
								</ul><!-- .section-list -->
							</div><!-- .custom-archive-item -->

							<div class="custom-archive-item">
								<h3 class="section-title"><?php _e( 'Archive By Year', 'tdmagazine' ); ?></h3>
								<ul class="section-list">
									<?php wp_get_archives( 'type=yearly' ); ?>
								</ul><!-- .section-list -->
							</div><!-- .custom-archive-item -->

							<div class="custom-archive-item">
								<h3 class="section-title"><?php _e( 'Pages', 'tdmagazine' ); ?></h3>
								<ul class="section-list">
								 <?php
									$pages = get_pages();
									foreach ( $pages as $page ) {
										echo '<li><a href="'.get_page_link( $page->ID ).'">'.$page->post_title.'</a></li>';
									  }
								 ?>
								</ul><!-- .section-list -->
							</div><!-- .custom-archive-item -->

							<div class="custom-archive-item">
								<h3 class="section-title"><?php _e( 'Categories', 'tdmagazine' ); ?></h3>
								<ul class="section-list">
								<?php
									$args = array(
										'orderby' => 'name',
										'order' => 'ASC'
									);

									$categories = get_categories( $args );
									foreach( $categories as $category ) {
										echo '<li><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s", "tdmagazine" ), $category->name ) . '" ' . '>' . $category->name.'</a> ('.$category->count.') </li>';
									}
								?>
								</ul><!-- .section-list -->
							</div><!-- .custom-archive-item -->

							<div class="custom-archive-item">
								<h3 class="section-title"><?php _e( 'Contributors', 'tdmagazine' ); ?></h3>
								<ul class="section-list">
								<?php
									$args = array(
										'show_fullname' => true,
										'optioncount' => true,
										'orderby' => 'post_count',
										'order' => 'DESC'
									);

									wp_list_authors($args);
								?>
								</ul><!-- .section-list -->
							</div><!-- .custom-archive-item -->

						</div><!-- .custom-archive-container -->

						<footer class="entry-footer">
							<?php edit_post_link( __( 'Edit', 'tdmagazine' ), '<span class="edit-link">', '</span>' ); ?>
						</footer><!-- .entry-footer -->

					</article><!-- #post-## -->

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

	<?php if( !tdmagazine_is_sidebar_hidden() ): ?>
	<div class="col-lg-4 col-md-4 sidebar-container">
		<?php get_sidebar(); ?>
	</div><!-- .sidebar-container -->
	<?php endif; ?>

</div><!-- .row -->

<?php get_footer(); ?>
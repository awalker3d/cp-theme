<?php
  /*
  Template Name: Authors List Page
  */
?>

<?php get_header(); ?>

<div class="row">
	<div id="primary" class="content-area <?php echo esc_attr( tdmagazine_get_blog_primary_class() ); ?>">
		<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>

						<?php tdmagazine_get_authors_list(); ?>

						<?php edit_post_link( __( 'Edit', 'tdmagazine' ), '<div class="edit-link">', '</div>' ); ?>
					</div><!-- .entry-content -->

			</article><!-- #post-<?php the_ID(); ?> -->
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

	<?php if( !tdmagazine_is_sidebar_hidden() ): ?>
	<div class="col-lg-4 col-md-4 sidebar-container">
		<?php get_sidebar(); ?>
	</div><!-- .sidebar-container -->
	<?php endif; ?>

</div><!-- .row -->

<?php get_footer(); ?>
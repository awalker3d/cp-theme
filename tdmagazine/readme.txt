== Description ==
tdMagazine is a responsive News/Magazine/Blog WordPress Theme.

== License: ==

* tdMagazine was built off the foundation of Underscores (_s):

- Underscores: http://underscores.me
- GPL License: http://www.gnu.org/licenses/gpl.txt

There are however some parts of this Theme which are not
GPL but they are GPL-Compatible.

See headers of JS files for further details.

* Font

- Font Awesome (http://fortawesome.github.com/Font-Awesome)
- SIL Open Font License: http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL

- Roboto (Font by Christian Robertson)
- Apache License, version 2.0: http://www.apache.org/licenses/LICENSE-2.0.html

- Roboto Slab (Font by Christian Robertson)
- Apache License, version 2.0: http://www.apache.org/licenses/LICENSE-2.0.html

* This theme uses Bootstrap Framework
 - Copyright 2013 Twitter, Inc
 - Licensed under the MIT : https://github.com/twbs/bootstrap/blob/master/LICENSE

== Theme Documentation: ==
Full documentation for tdMagazine Theme can be found with your theme download. See the Documentation folder for more details.

==  Theme Support: ==
If you have any questions, please contact me:
http://themeforest.net/user/taras_d
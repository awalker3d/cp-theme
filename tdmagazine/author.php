<?php
/**
 * The template for displaying Author archive.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package tdmagazine
 */

get_header(); ?>

<div class="row">
	<div id="primary" class="content-area <?php echo esc_attr( tdmagazine_get_blog_primary_class() ); ?>">
		<div id="content" class="site-content" role="main">

		<?php if ( have_posts() ) : ?>

			<?php tdmagazine_author_section(); ?>

			<?php if( tdmagazine_is_two_columns_layout() ): ?>
			<div class="content-grid row columns-blog">

				<?php while ( have_posts() ) : the_post(); ?>
					<div class="col-lg-6 col-md-6 post-box">
					<?php get_template_part( 'content', get_post_format() ); ?>
					</div><!-- .post-box -->
				<?php endwhile; ?>

			</div><!-- .content-grid -->
			<?php elseif( tdmagazine_is_three_columns_layout() ): ?>
			<div class="content-grid row columns-blog">

				<?php while ( have_posts() ) : the_post(); ?>
					<div class="col-lg-4 col-md-4 post-box">
					<?php get_template_part( 'content', get_post_format() ); ?>
					</div><!-- .post-box -->
				<?php endwhile; ?>

			</div><!-- .content-grid -->
			<?php elseif( tdmagazine_is_inline_layout() ): ?>

				<div class="inline-blog">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'inline' ); ?>
				<?php endwhile; ?>
				</div><!-- .inline-blog -->

			<?php else: ?>

				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', get_post_format() ); ?>
				<?php endwhile; ?>

			<?php endif; ?>

			<?php tdmagazine_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

	<?php if( !tdmagazine_is_sidebar_hidden() ): ?>
	<div class="col-lg-4 col-md-4 sidebar-container">
		<?php get_sidebar(); ?>
	</div><!-- .sidebar-container -->
	<?php endif; ?>

</div><!-- .row -->

<?php get_footer(); ?>
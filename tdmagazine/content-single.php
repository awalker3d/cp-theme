<?php
/**
 * @package tdmagazine
 */
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php if( has_post_format( 'gallery' ) ): ?>

			<?php if ( tdmagazine_has_post_slideshow() ): ?>
			<div class="post-slideshow">
				<?php tdmagazine_post_slideshow(); ?>
			</div><!-- .post-slideshow -->
			<?php endif; ?>

		<?php elseif( has_post_format( 'video' ) ): ?>

			<?php if ( tdmagazine_has_post_video() ): ?>
			<div class="post-video">
				<?php tdmagazine_post_video(); ?>
			</div><!-- .post-video -->
			<?php endif; ?>

		<?php else: ?>

			<?php if ( has_post_thumbnail() ): ?>
			<div class="post-thumb">
				<?php the_post_thumbnail(); ?>

				<?php if( tdmagazine_featured_image_credits() ): ?>
				<div class="post-thumb-credits">
					<?php echo tdmagazine_featured_image_credits(); ?>
				</div><!-- .post-thumb-credits -->
				<?php endif; ?>
			</div><!-- .post-thumb -->
			<?php endif; ?>

		<?php endif; ?>

		<header class="entry-header">
			<h2 class="entry-title"><?php the_title(); ?></h2>

			<div class="entry-meta top">
				<?php tdmagazine_posted_on(); ?>
				<?php edit_post_link( __( 'Edit', 'tdmagazine' ), '<span class="edit-link"> / ', '</span>' ); ?>
			</div><!-- .entry-meta -->

			<div class="entry-category">
				<?php tdmagazine_categories(); ?>
			</div><!-- .entry-category -->

			<?php if( tdmagazine_is_share_buttons_top() ): ?>
			<?php tdmagazine_share_buttons(); ?>
			<?php endif; ?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php the_content(); ?>
			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'tdmagazine' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->

		<footer class="entry-meta entry-footer">
			<div class="entry-tags">
				<?php tdmagazine_tags(); ?>
			</div><!-- .entry-tags -->

			<?php if( tdmagazine_is_share_buttons_bottom() ): ?>
			<?php tdmagazine_share_buttons(); ?>
			<?php endif; ?>
		</footer><!-- .entry-meta -->
</article><!-- #post-## -->

<?php tdmagazine_author_section(); ?>

<?php tdmagazine_related_posts(); ?>

<?php tdmagazine_ad_section( 'post' ); ?>


<?php
/**
 * @package tdmagazine
 */
?>

<article <?php post_class( 'other-posts clearfix' ); ?>>
	<header class="entry-header">
		<?php if ( has_post_thumbnail() ): ?>
			<a href="<?php echo esc_url( get_permalink() ); ?>" class="thumb-link alignleft">
			<?php the_post_thumbnail( 'thumb-image' ); ?>
			</a><!-- .thumb-link -->
		<?php endif; ?>

		<h2 class="entry-title"><a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta top">
			<?php tdmagazine_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->
</article><!-- .hentry -->